
package xigo_test

import (
	"fmt"
	"os"
	"strings"
	"testing"	
	"time"

	"bitbucket.org/EScottDaniels/sketch/xigo"
)

func Test_single_image( t *testing.T ) {

	gc, err := xigo.Mk_xigo_gc( ",900,1200,XI Sketch Test" )
	if gc == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] cant allocate window: %s\n", err )
		return
	}
	gc.Set_scale( 2, 2 )

	im, err := gc.Mk_bounded_image( "/tmp/image_test.jpg", 300, 300 ) 
	if err != nil  {
		fmt.Fprintf( os.Stderr, "[FAIL] load of image failed: %s\n", err )
		t.Fail()
		return
	}	

    gc.Set_colour( "white" )		// this will show if we have issues to ensure we see what we want
    gc.Set_fill_attrs("white", 0 )
    gc.Draw_rect( 9, 9, 301, 301, true )

	gc.Draw_image( 10, 10, im )		// draw image at x,y

	gc.Show( )
	fmt.Fprintf( os.Stderr, ">>>> sleeping\n" )
	time.Sleep( 100 * time.Second )
}

func Test_stacked_image( t *testing.T ) {
	gc, err := xigo.Mk_xigo_gc( ",900,1200,XI Stacked Image Test" )
	if gc == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] cant allocate window: %s\n", err )
		return
	}

	image_names := "/tmp/simage1_test.gif,/tmp/simage2_test.gif"
	images := strings.Split( image_names, "," )

	im, err := gc.Mk_stacked_image( images, 400, 400 ) 
	if err != nil  {
		fmt.Fprintf( os.Stderr, "[FAIL] load of image failed: %s\n", err )
		t.Fail()
		return
	}	

    gc.Set_colour( "white" )		// this will show if we have issues to ensure we see what we want
    gc.Set_fill_attrs("white", 0 )
    gc.Draw_rect(9, 9, 401, 401, true)

	gc.Draw_image( 10, 10, im )		// draw image at x,y

	gc.Show( )
	fmt.Fprintf( os.Stderr, ">>>> sleeping\n" )
	time.Sleep( 100 * time.Second )
}
