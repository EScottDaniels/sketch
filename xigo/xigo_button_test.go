
package xigo_test

import (
	"fmt"
	"os"
	"testing"	
	"time"

	"bitbucket.org/EScottDaniels/sketch/xigo"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

func event_sucker( gc *xigo.Xigo_gc, ch chan *sktools.Interaction ) {
	menu_hidden := true

	for ;; {
		e := <- ch

		switch e.Kind {
			case sktools.IK_SB_RELEASE:
				switch e.Data {				// switch on button id
					case 0:
						fmt.Printf( "event received: event=%d data=%d  x=%.2f y=%.2f\n", e.Kind, e.Data, e.X, e.Y )
						menu_hidden = ! menu_hidden
						if menu_hidden {
							hide_menu( gc )
						} else {
							show_menu( gc, ch )
						}
				
					default:
						fmt.Printf( "event received and ignored: event=%d data=%d  x=%.2f y=%.2f\n", e.Kind, e.Data, e.X, e.Y )
				}
		}
	}
}

/*
	Cause a menu subpage to be displayed.
*/
func  show_menu( gc *xigo.Xigo_gc, echan chan *sktools.Interaction ) {

	height := 20
	width := 80
	x := 3
	y := 3
	gc.Select_subpage( "root" )
	gc.Mk_subpage( "menu", 30, 20, float64((height * 10)+30),  float64(width + 4) * 2.0  )

	gc.Select_subpage( "menu" )
	//gc.Set_page_colour( "#600060" )
	gc.Clear_subpage( "#600060" )

	for i := 0; i < 10; i++ {
		txt := fmt.Sprintf( "button %d", i )
		gc.Add_button( 100+i, x, y, height, width, txt, "#606060", "white", xigo.BT_STICKY, true )
		y += height + 3
	}

	gc.Select_subpage( "menu" )
	gc.Add_listener( echan )
	gc.Select_subpage( "root" )
}

/*
	Causes the menu in a subwindow to be removed.
*/
func hide_menu( gc *xigo.Xigo_gc ) {
	gc.Select_subpage( "root" )
	gc.Delete_subpage( "menu" )
}

func Test_buttons( t *testing.T ) {
	var echan  chan *sktools.Interaction

	gc, err := xigo.Mk_xigo_gc( ",900,1200,XI Sketch Button Test" )
	if gc == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] cant allocate window: %s\n", err )
		return
	}

	echan = make( chan *sktools.Interaction, 1024 )
	go event_sucker( gc, echan )

	x := 10
	y := 2

	gc.Add_button( 0, x, y, 20, 50, "Main", "#606060", "#a0a0a0", xigo.BT_STICKY, false )
	gc.Add_button( 1, x+52, y, 20, 50, "Left", "#606060", "#a0a0a0", xigo.BT_STICKY, true )
	gc.Add_button( 2, x+104, y, 20, 50, "Right", "#606060", "#a0a0a0", xigo.BT_STICKY, false )
	gc.Add_listener( echan )

	for ;; {
		gc.Drive( )						// drive any events generated
    	gc.Set_colour( "green" )		// this will show if we have issues to ensure we see what we want
	
		//if gc.Select_subpage( "menu" ) {
			//gc.Clear_subpage( "#606060" )
		//}

		gc.Show( )
		time.Sleep( 10 * time.Millisecond )
	}
}
