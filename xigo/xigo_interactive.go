/*
	Mnemonic:	Xigo_interactive
	Abstract:	Support which provides interactive controls and callback channels
				for:
					- screen (soft) button actions (press/release)
					- mouse clicks (press/release)
					- slider adjustment
				

	Date:		09 December 2018
	Author:		E. Scott Daniels
*/

package xigo

/*
#cgo CFLAGS:  -I /usr/include -I /usr/include/freetype2
#cgo LDFLAGS:  -lxi -lXft -lfontconfig -lX11-xcb -lXext -lX11 -lm

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xdbe.h>

#include <xi/xi.h>

// ---- protos of callback wrapper functions -----
int cb_mbutton_press( void* data, int display, int window, void* ev );
int cb_mbutton_rel( void* data, int display, int window, void* ev );
typedef int (*intFunc)();

*/
import "C"

import (
	"fmt"
	"os"

	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)



/*
	Adds a button to the active window described by the gc. Because the C code cannot call one of
	our go functions as a callback, we pass nil values for the press/release callbacks.  We will
	set a generic callback function for each type (press/release) when a listener is added and that
	will drive the Go function that will stuff an event onto the proper channel.

	Sketch does NOT implement a button class concept as the class is set to the window number 
	allowing us to paint all buttons in a window by class.  If radio buttons are desired, then
	the user programme must create a subpage, and draw radio buttons there as they will be properly
	'classed' from a push one and the currently depressed button pops up.

	The pushed parm allows a sticky or radio button to start out as depressed. It is ignored for 
	spring buttons.
*/
func( xgc *Xigo_gc ) Add_button( bid int, x int, y int, height int, width int, label string, colour string, txt_colour string, kind int, pushed bool ) {
	window := xgc.active.Get_win()

	xgc.Lock()
	colour_id := xgc.add_colour( colour )		// translate to colour map IDs
	tcolour_id := xgc.add_colour( txt_colour )

	C.XIaddbutton( window, C.int( x ), C.int( y ), C.int ( height ), C.int( width ), nil, nil, nil, colour_id, tcolour_id,
			C.CString( label ), C.int( bid ), C.int( kind ), window,  C.XI_OPT_FLUSH )		// buttons are classed by window

	if (C.int( kind ) != C.XI_SPRING_BUTTON) && pushed {
		C.XImodbutton( window, 0, 0, 0, 0, nil, nil, 0, 0, nil, C.int( bid ), window, C.XI_MOD_PRESS )
	}
	xgc.Unlock()
}

/*
	Adds the callback listener channel that a goroutine is listening on for callback's related to this gc.
	This function will also register the callback functions with XI. 

	Because pointer data cannot be passed to C functions, there is only one listener that will sent
	events for any gc. 
*/
func( xgc *Xigo_gc ) Add_listener( lchan chan *sktools.Interaction ) {

	xgc.listener = lchan;				// reference the channe, then ensure that all subpages set callbacks for their windows
	xgc.active.Add_listener( xgc )		// to selected page and all subpages
}


/*
	Callback to channel handlers.  The callback funcitons in xigo_c.go which are registered with 
	XI, cannot reference any Go information (specifically pointers and channels). In order to 
	place events onto the user's channel, these GO functions are known to the callback handlers
	and are invoked when an event happens. The global variable used to map window to gc, is 
	how these functions connect the user's listener channel to the window in which the event
	occurred.
*/

/*
	Called when a screen/soft button is pressed
*/
//export cbch_button_press
func cbch_button_press( win C.int, bid C.int ) ( state C.int ) {
	gc := win2xgc[int( win )]

	if gc == nil || gc.listener == nil {
		fmt.Fprintf( os.Stderr, "[ERR] gc or listener for gc was nil!\n" )
		return C.XI_OK
	}

    ev := &sktools.Interaction {
        Data: int( bid ),
        Kind: sktools.IK_SB_PRESS,
    }

    gc.listener <- ev
    return C.XI_OK
}

/*
	Called when a screen/soft button is released.
*/
//export cbch_button_rel
func cbch_button_rel( win C.int, bid C.int ) ( state C.int ) {
	gc := win2xgc[int( win )]

    ev := &sktools.Interaction {
        Data: int( bid ),
        Kind: sktools.IK_SB_RELEASE,
    }

    gc.listener <- ev
    return C.XI_OK
}

/*
	Called when a mouse button is released when it is NOT on a soft/screen
	button.
*/
//export cbch_mouse_rel
func cbch_mouse_rel( win C.int, x C.int, y C.int, mbutton C.int ) ( state C.int ) { 
	gc := win2xgc[int( win )]
	if gc == nil {
		return C.XI_OK				// nothing can happen if we report failure, so don't
	}

    ev := &sktools.Interaction {
		X: float64( x ),
		Y: float64( y ),
		Data: int( mbutton ),
        Kind: sktools.IK_M_RELEASE,
    }

    gc.listener <- ev
    return C.XI_OK
}

/*
	Called when a mouse button is pressed when it is NOT on a soft/screen
	button.
*/
//export cbch_mouse_press
func cbch_mouse_press( win C.int, x C.int, y C.int, mbutton C.int ) ( state C.int ) { 
	gc := win2xgc[int( win )]

    ev := &sktools.Interaction {
		X: float64( x ),
		Y: float64( y ),
		Data: int( mbutton ),
        Kind: sktools.IK_M_PRESS,
    }

    gc.listener <- ev
    return C.XI_OK
}

func ( xgc *Xigo_gc ) Drive( ) {
	C.XIdriver( C.XI_MODE_POLL )
}

/*
	Release_button causes a radio/sticky button to be 'popped out.' 
*/
func( xgc *Xigo_gc ) Release_button( wname string, bid int,  ) {
	if xgc == nil {
		return
	}

	xgc.Select_subpage( wname )
	window := xgc.active.Get_win()

	xgc.Lock()
	C.XImodbutton( window, 0, 0, 0, 0, nil, nil, 0, 0, nil, C.int( bid ), window, C.XI_MOD_RELEASE )
	xgc.Unlock()
}

/*
	Press causes a radio/sticky button to be 'pressed in.' 
*/
func( xgc *Xigo_gc ) Press_button( wname string, bid int,  ) {
	if xgc == nil {
		return
	}

	xgc.Select_subpage( wname )
	window := xgc.active.Get_win()

	xgc.Lock()
	C.XImodbutton( window, 0, 0, 0, 0, nil, nil, 0, 0, nil, C.int( bid ), window, C.XI_MOD_PRESS )
	xgc.Unlock()
}


