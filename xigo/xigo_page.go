/*
	Mnemonic:	Xigo_page
	Abstract:	Implements the page oriented Sketch API.
	Date:		15 July 2018
	Author:		E. Scott Daniels
*/

package xigo


// ---------- C interface preamble -------------------------------------------------
/*
	Because we export functions in this module we canNOT add external functions
	to the C code below.  External functions that handle real mouse button presses,
	and are given as function pointers here, are defined in xgo_page_ext.go. Their 
	prototypes must be here.
*/

/*
#cgo CFLAGS: -I /usr/include -I /usr/include/freetype2 
#cgo LDFLAGS: -lxi -lX11 -lm

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xdbe.h>

#include <xi/xi.h>

// ---- protos of callback wrapper functions -----
int cb_mbutton_press( void* data, int display, int window, void* ev );
int cb_mbutton_rel( void* data, int display, int window, void* ev );
typedef int (*intFunc)();
*/
import "C"

import (
	"fmt"
	"unsafe"
)

/*
	Used to manage both a root page (window) and a subpage (window in its own right,
	but within a window). 
*/
type Xigo_page struct {
	name	string
	x		int							// position of subpage so we can move for scaling
	y		int
	adj_x	int							// last adjusted x,y after move
	adj_y	int
	height	int
	width	int
	title	string
	window	C.int						// window number from XI
	pages	map[string]*Xigo_page		// sub pages
	colour C.int						// the window's active colour
	pg_colour C.int						// page (background) colour
	visible	bool						// painted only if true
	locked bool							// page scale and position is locked
}

/*
	Mk_page creates a main page which in the X world is a root window.
	The title is of the form <internal-name>:<window-title> where the internal
	name is used to make the page active and the title is displayed on the 
	window border.
*/
func ( gc *Xigo_gc ) Mk_page( pname string, height int, width int, title string ) ( error ) {
	if gc == nil {
		return fmt.Errorf( "graphics context was nil" )
	}

	if win2xgc == nil {								// must be able to map window back to gc
		win2xgc = make( map[int] *Xigo_gc, 27 )
	}

	mark_default := false
	if len( gc.pages ) == 0 {
		mark_default = true				// first page is also marked the default page
	}

	ctitle := C.CString( title ) 
	defer C.free( unsafe.Pointer( ctitle ) )

	gc.pages[pname] = &Xigo_page {
		name:	pname,
		height: height,
		width:	width,
		title:	title,
		visible:	true,
		pages:	make( map[string]*Xigo_page, 17 ),
		window:	C.XIopenw( gc.display, C.XI_PARENT, 0, 0, C.int( width ), C.int( height ), C.XI_WHITE, C.XI_BLACK, ctitle, 0 ),
	}

	C.XIxlate( gc.pages[pname].window, 0, 0 )
	win2xgc[int( gc.pages[pname].window )] = gc			// supply window number to xgc mapping

	if mark_default && pname != "default" {
		gc.pages["default"] = gc.pages[pname]
		gc.active = gc.pages[pname]
	}
	
	return nil
}

/*
	Ensure that the callback funciton will be driven for this subpage window and that
	the window will map to the given gc.
*/
func ( p *Xigo_page ) Add_listener( gc *Xigo_gc ) {
	if p == nil {
		return
	}
	//fmt.Fprintf( os.Stderr, "add listeners for page=%s win=%d\n",  p.name, int( p.window ) )
	
	win2xgc[int( p.window ) ] = gc
   	C.XIcbreg( p.window, C.ButtonRelease, C.intFunc( C.cb_mbutton_rel ), nil );  // real mouse button pres/release; call functions in xigo.c.go
   	C.XIcbreg( p.window, C.ButtonPress, C.intFunc( C.cb_mbutton_press ), nil );

	for _, subp := range p.pages {
		subp.Add_listener( gc )
	}
}

/*
	Add_subpage creates a new subpage and places it into the drawing order at the "top" (last drawn).
*/
func ( p *Xigo_page ) Add_subpage( pname string, x int, y int, height int, width int ) ( sp *Xigo_page ) {
	if p == nil {
		return 
	}

	cpname := C.CString( pname ) 
	defer C.free( unsafe.Pointer( cpname ) )

	spage := &Xigo_page {
		name:	pname,
		x:		x,
		y:		y,
		height: height,
		width:	width,
		title:	pname,
		visible:	true,
		window: C.XIopenw( p.window, C.XI_CHILD, C.int( x ), C.int( y ), C.int( width ), C.int( height ), C.XI_BLACK, C.XI_BLACK, cpname, 0 ),
		pages:	make( map[string]*Xigo_page, 17 ),
	}

	p.pages[pname] = spage
	return spage
}

/*
	Lock causes the page's size and scale to become fixed (locked) and will not 
	be affected by scale and move requests.
*/
func ( p *Xigo_page ) Lock( ) {
	if p == nil {
		return
	}

	p.locked = true
}

/*
	Move will change the x,y location of the subpage by the delta x,y passed in
	applying the scale factor after adjusting the point.  If only adjusting the
	scale, then a delta of 0,0  should be passed.
*/
func ( p *Xigo_page ) Move( delta_x int, delta_y int, scale float64 ) {
	if p == nil || p.locked {
		return
	}

	p.adj_x = delta_x			// capture movement; used if we scale later
	p.adj_y = delta_y
	new_x := int( (scale * (float64( p.x )) + float64( delta_x )) )
	new_y := int( (scale * (float64( p.y )) + float64( delta_y )) )

	C.XImovew( p.window, C.int( new_x ), C.int( new_y ) )
}

/*
	Move_subpages causes all subpages to be moved relative to their original
	x,y location.
*/
func ( p *Xigo_page ) Move_subpages( dx int, dy int, scale float64, recursive bool ) {
	if p == nil {
		return 
	}

	for _, sp := range p.pages {
		sp.Move( dx, dy, scale )
		if recursive {
			sp.Move_subpages( dx, dy, scale, true )
		}
	}
}

/*
	Scale causes the window size to be changed by scaling the defined window
	height/width using the factor passed in.
*/
func ( p *Xigo_page ) Scale( scale float64 ) {
	if p == nil || p.locked {
		return 
	}

	new_height := C.int( float64( p.height ) * scale )
	new_width := C.int( float64( p.width ) * scale )
	new_x := int( (scale * (float64( p.x )) + float64( p.adj_x )) )
	new_y := int( (scale * (float64( p.y )) + float64( p.adj_y )) )

	C.XIresizew( p.window, new_height, new_width )
	C.XImovew( p.window, C.int( new_x ), C.int( new_y ) )
}

/*
	Scale_subpages causes all subpages to this page to be scaled.  If recursive is 
	true, then for each subpage, we scale their subpages too.
*/
func ( p *Xigo_page ) Scale_subpages( scale float64, recursive bool ) {
	if p == nil {
		return 
	}

	for _, sp := range p.pages {
		sp.Scale( scale )
		if recursive {
			sp.Scale_subpages( scale, true )
		}
	}
}

/*
	Clears the page by filling with the current colour, and calls clear for
	all subpages.
*/
func ( p *Xigo_page ) Clear( xgc *Xigo_gc, clear_all bool ) {
	if p == nil {
		return
	}

	if clear_all {
		for _, pg := range p.pages  {
			pg.Clear( xgc, true )
		}
	}

	xgc.force_win_colour( p.window, p.pg_colour )
	C.XIdrawr( p.window, 0, 0, C.int( p.height ), C.int( p.width ), C.XI_OPT_BB | C.XI_OPT_FILL )	// clear this page
	xgc.force_win_colour( p.window, p.colour )
}

/*
	Close does whatever is needed to close the page including closing any open subpages.
*/
func ( p *Xigo_page ) Close( ) {
	if p == nil {
		return
	}

	for spname, sp := range p.pages {
		sp.Close()
		delete( p.pages, spname )
	}

	C.XIdel_buttons( p.window, p.window )
	C.XIclosew( p.window )
	p.window = -1
}

/*
	Finds the named subpage and returns it to the caller.
*/
func( p *Xigo_page ) Get_subpage( pname string ) ( sp *Xigo_page ) {
	if p == nil {
		return nil
	}
	return p.pages[pname]
}

/*
	Get_win returns the page's window for drawing operations.
*/
func ( p *Xigo_page ) Get_win( ) ( C.int ) {
	if p == nil {
		return -1
	}

	return p.window
}

/*
	Sets the visibility of the page/subpage to false.
*/
func ( p *Xigo_page ) Set_invisible( ) {
	if p == nil {
		return
	}

	p.visible = false
}

/*
	Sets the visibility of the page/subpage to true.
*/
func ( p *Xigo_page ) Set_visible( ) {
	if p == nil {
		return
	}

	p.visible = true
}

/*
	Show will cause the drawing buffer for this page to be swapped (making it
	visible), and if there are any sub pages, will invoke their show function
	to do the same. Sub pages are rendered in reverse creation order.
*/
func ( p *Xigo_page ) Show( ) {
	if p == nil {
		return
	}

	if !p.visible {
		return
	}

	C.XIdispbutton( p.window, p.window, C.XI_OPT_CLASS | C.XI_OPT_FLUSH | C.XI_OPT_BB )		// write buttons to the buffer just before painting
	C.XIswap_buffers( p.window )

	for _, sp := range p.pages {
		sp.Show()
	}

	C.XIsetcolour( p.window, p.pg_colour )
	//fmt.Fprintf( os.Stderr, ">>>>>> clearing page with %d\n", int( p.pg_colour ) );
	C.XIdrawr( p.window, 0, 0, C.int( p.height ), C.int( p.width ), C.XI_OPT_BB | C.XI_OPT_FILL )	// immediately after db flip, set bg colour of page
	C.XIsetcolour( p.window, p.colour )
}

/*
	Set_colour sets the currently active colour for drawing.
*/
func (p *Xigo_page ) Set_colour( colour C.int ) {
	if p == nil {
		return
	}

	p.colour = colour
}

/*
	Set_page_colour sets the page colour used when clearing the page.
*/
func (p *Xigo_page ) Set_page_colour( colour C.int ) {
	if p == nil {
		return
	}

	p.pg_colour = colour
}

func (p *Xigo_page ) Get_colour( ) C.int {
	if p == nil {
		return 0
	}

	return p.colour
}

func (p *Xigo_page ) Get_page_colour( ) C.int {
	if p == nil {
		return 0
	}

	return p.pg_colour
}


/*
	Unock causes the page's size and scale to become variable (unlocked) and will 
	be affected by scale and move requests.
*/
func ( p *Xigo_page ) Unlock( ) {
	if p == nil {
		return
	}

	p.locked = false
}

