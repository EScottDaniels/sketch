/*
	Mnemonic:	Xgo_image
	Abstract:	Support to load/display an image.
				
	Date:		16 September 2018
	Author:		E. Scott Daniels
*/

package xigo


// ---------- C interface preamble -------------------------------------------------
/*
	Because we export functions in this module we canNOT add external functions
	to the C code below.  External functions that handle real mouse button presses,
	and are given as function pointers here, are defined in xgo_page_ext.go. Their 
	prototypes must be here.
*/

/*
#cgo CFLAGS: -I /usr/include -I /usr/include/freetype2 
#cgo LDFLAGS: -lxi -lXft -lfontconfig -lX11-xcb -lXext -lX11 -lm

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xdbe.h>

#include <xi/xi.h>

// ---- protos of callback wrapper functions -----
//int cb_mbutton_press( void* data, int display, int window, void* ev );
//int cb_mbutton_rel( void* data, int display, int window, void* ev );
//typedef int (*intFunc)();

*/
import "C"

import (
	"fmt"
	"os"
	"image"
	_ "image/png"				// pull just to run initialisation so that we can load these types
	_ "image/jpeg"
	_ "image/gif"
	"unsafe"
)

type Xigo_image struct {
	fname	string						// where we get the image (reload support)
	// broken with go 1.12 and beyond pxmap	*_Ctype_struct__XImage		// the bits that are drawable by XI
	pxmap	*C.struct__XImage		// the bits that are drawable by XI
	height	int
	width	int
}

// -----------------------------------------------------------------------------------

/*
	Load an image and scale it.
*/
func( xgc *Xigo_gc ) Mk_scaled_image( fname string, scale float64 ) ( image interface{}, e error ) {
	pm, height, width, err := load_pixmap( fname )
	
	if err != nil {
		return nil, err
	}

	window := xgc.active.Get_win()
	depth := C.int( 0 )			// xi newimage gets depth from display, so not needed, but must be passed
	bpp := C.int( 4 )			// bytes per pix rgb/a

	xpix := C.XInewimage( window, C.int( height ), C.int( width ), depth, unsafe.Pointer( &pm[0] ), bpp, C.double( scale ) )
	image = &Xigo_image {
		height:	height,
		width:	width,
		pxmap:	xpix,
		fname:	fname,
	}

	return image, nil
}

/*
	Load an image and scale it based on a bounding box. The image will be scaled such 
	that the largest dimension will utialise the entire bounding box in that direction.
*/
func( xgc *Xigo_gc ) Mk_bounded_image( fname string, max_height int, max_width int ) ( image interface{}, e error ) {
	pm, height, width, err := load_pixmap( fname )
	
	if err != nil {
		return nil, err
	}

	max_height = xgc.scale_int( max_height )
	max_width = xgc.scale_int( max_width )

	window := xgc.active.Get_win()
	w_scale := float64( 1.0 )	// initially assume 1:1
	h_scale := float64( 1.0 )
	scale := float64( 1.0 )

	if width > max_width {
		w_scale = float64( max_width ) / float64( width )		// potential scale if width fits
	}

	if height > max_height {
		h_scale = float64( max_height ) / float64( height )		// potential scale if height fits
	}

	if w_scale < h_scale {		// pick smaller scale as that is what is needed
		scale = w_scale
	} else {
		scale = h_scale
	}

	depth := C.int( 0 )			// xi newimage gets depth from display, so not needed, but must be passed
	bpp := C.int( 4 )			// bytes per pix rgb/a

	image = &Xigo_image {
		height:	height,
		width:	width,
		pxmap:	C.XInewimage( window, C.int( height ), C.int( width ), depth, unsafe.Pointer( &pm[0] ), bpp, C.double( scale )  ),
		fname:	fname,
	}

	pm = nil

	return image, nil
}


/*
	Mk_stacked_image will load numtiple images from different files stacking them into to 
	same pix map.
*/
func( xgc *Xigo_gc ) Mk_stacked_image( fnames []string, max_height int, max_width int ) ( image interface{}, e error ) {
	pm, height, width, err := load_stacked_pixmap( fnames )
	
	if err != nil {
		return nil, err
	}

	window := xgc.active.Get_win()
	w_scale := float64( 1.0 )	// initially assume 1:1
	h_scale := float64( 1.0 )
	scale := float64( 1.0 )

	if width > max_width {
		w_scale = float64( max_width ) / float64( width )		// potential scale if width fits
	}

	if height > max_height {
		h_scale = float64( max_height ) / float64( height )		// potential scale if height fits
	}

	if w_scale < h_scale {		// pick smaller scale as that is what is needed
		scale = w_scale
	} else {
		scale = h_scale
	}

	depth := C.int( 0 )			// xi newimage gets depth from display, so not needed, but must be passed
	bpp := C.int( 4 )			// bytes per pix rgb/a

	image = &Xigo_image {
		height:	height,
		width:	width,
		pxmap:	C.XInewimage( window, C.int( height ), C.int( width ), depth, unsafe.Pointer( &pm[0] ), bpp, C.double( scale )  ),
		fname:	fnames[0],
	}

	pm = nil

	return image, nil
}


/*
	Load the image and then convert the rgb/a into a pixmap that XI can use.
	Returns the raw pix map and it's dimensions.
*/
func load_pixmap( fname string ) ( pm []byte, height int, width int,  e error ) {
	reader, err := os.Open( fname )
	if err != nil {
		return nil, 0, 0, err
	}

	m, _, err := image.Decode( reader )
	if err != nil {
		return nil, 0, 0, err
	}

	bounds := m.Bounds()

	pm = make( []byte, bounds.Max.Y * bounds.Max.X * 4 )		// 4 channel -- rgb/a
	i := 0;
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {		// per golang example, better memory alignment when doing y first
		for x := bounds.Min.X; x < bounds.Max.X; x++ {

			r, g, b, a := m.At( x, y ).RGBA()				// values are 0..65K
			pm[i] = byte( r >> 8 )						// shift by 8 reduces range to 0..255
			pm[i+1] = byte( g >> 8 )
			pm[i+2] = byte( b >> 8 )
			pm[i+3] = byte( a >> 8 )
			i += 4
		}
	}

	return pm, bounds.Max.Y - bounds.Min.Y, bounds.Max.X - bounds.Min.X, nil
}

/*
	Load_stacked_pixmap will read n image files and merge them (image 0 is the
	bottom).  Merge is done such that black pixles are ignored and not written
	from the source image into the merged image.
*/
func load_stacked_pixmap( fnames []string ) ( pm []byte, height int, width int,  e error ) {
	var (
		base_x	int
		base_y	int
	)

	//return load_pixmap( fnames[0] )

	nloaded := 0
	for fi := 0; fi < len( fnames ); fi++ {
		fname := fnames[fi]
		reader, err := os.Open( fname )
		if err != nil {
			continue
		}

		m, _, err := image.Decode( reader )
		if err != nil {
			fmt.Fprintf( os.Stderr, "error: %s: %s\n", fname, err )
			continue
		}
		bounds := m.Bounds()

		if pm == nil {
			base_x = bounds.Max.X - bounds.Min.X
			base_y = bounds.Max.Y - bounds.Min.Y
			pm = make( []byte, bounds.Max.Y * bounds.Max.X * 4 )		// 4 channel -- rgb/a
		} else {
			if bounds.Max.X - bounds.Min.X > base_x || bounds.Max.Y - bounds.Min.Y > base_y {
				continue
			}	
		}

		nloaded++
		i := 0;
		for y := bounds.Min.Y; y < bounds.Max.Y; y++ {		// per golang example, better memory alignment when doing y first
			for x := bounds.Min.X; x < bounds.Max.X; x++ {
	
				r, g, b, a := m.At( x, y ).RGBA()				// values are 0..65K
				if r > 0xff || b > 0xff || g > 0xff {
					pm[i] = byte( r >> 8 )						// shift by 8 reduces range to 0..255
					pm[i+1] = byte( g >> 8 )
					pm[i+2] = byte( b >> 8 )
					pm[i+3] = byte( a >> 8 )
				}
				i += 4
			}
		}
	}

	//return pm, bounds.Max.Y - bounds.Min.Y, bounds.Max.X - bounds.Min.X, nil
	if nloaded > 0 {
		return pm, base_y, base_x, nil
	} 

	for i := 0; i < len( fnames ); i++ {
		fmt.Fprintf( os.Stderr, "attempted to load: %s\n", fnames[i] )
	}

	return nil, 0, 0, fmt.Errorf( "unable to load any file in stack: %s", fnames[0] )
}


/*
	Close_image will do any cleanup that is needed. In the case of XI we need it to 
	release the pixmap allocated by this code, and the allocated image in the XI
	world to prevent leaks.
*/
func( xgc *Xigo_gc ) Close_image( iimage interface{} ) {
	if xgc == nil || iimage == nil {
		return
	}

	img, ok := iimage.( *Xigo_image )
	if ! ok {
		return
	}

	C.XIfreeimage( img.pxmap )
}

/*
	Draw_image accepts an Xigo_image pointer and draws it into the xi environment
	using the associated gc. It is positions such that the top left corner (ulc) is
	positioned at xo,yo.
*/
func( xgc *Xigo_gc ) Draw_image( xo float64, yo float64, iimage interface{} ) {
	if xgc == nil || iimage == nil {
		return
	}

	img, ok := iimage.( *Xigo_image )
	if ! ok {
		return
	}

	window := xgc.active.Get_win()

	xgc.Lock()
	defer xgc.Unlock()

	C.XIdrawimage( window, img.pxmap, xgc.scale_cint( xo ), xgc.scale_cint( yo ), 0, 0 ) // 0,0 is the point in the image to place at xo,yo
}
