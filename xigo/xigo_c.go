
/*
	Mnemonic:	xigo_c.go
	Abstract:	C functions which can be provided to the XI library as callbacks.
	Date:		9 December 2018
	Author:		E. Scott Daniels
*/

package xigo

/*
#cgo CFLAGS:
#cgo LDFLAGS: -v -v -lxi -lfreetype  -lX11 -lm

//LDFLAGS: -L /data/arm_root/lib -lxi -lfreetype  -lX11 -lm



#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xdbe.h>

#include <xi/xi.h>

// protos for go cb to chan interface functions
extern int cbch_button_rel( int win, int bid );
extern int cbch_button_press( int win, int bid ); 
extern int cbch_mouse_press( int win, int x, int y, int button );	// mouse click not on a screen/soft button
extern int cbch_mouse_rel( int win, int x, int y, int button );

// --- mouse button callback functions ---------------------------------------
//

//
//	These functions are registered when a listener is registered for a gc. They 
//	are called for any mouse button related event (press or relesase) and will
//	invoke the XI button manger to determine if the event happened while the pointer
//	was placed on a screen/soft button.  If so, then a cbch_button_*() funciton 
//	is invoked to create an event for the listener.
//
//	If a soft/screen button was not under the pointer, then a cbch_mouse_*()
//	function is invoked to create a mouse click event which is passed to the 
//	listener. 
//
//	The two tier approach is needed because the C functions are not allowed to have
//	Go pointers, and thus it's not possible for these functions to know how to 
//	put data onto the listener's channel, but they can call a Go function that can
//	determine the channel.

int cb_mbutton_rel( void* data, int display, int window, void* event ) {
	int bid;
	XButtonEvent* e;

	bid = XIbtnmgr( window, event );
	if( bid != XI_ERR_NOBUTTON ) {
		cbch_button_rel( window, bid );
		return XI_OK;
	}
	
	e = (XButtonEvent *) event;
	cbch_mouse_rel( window, e->x, e->y, e->button );
	return XI_OK;
}

int cb_mbutton_press( void* data, int display, int window, void* event ) {
	int bid;
	XButtonEvent* e;

	bid = XIbtnmgr( window, event );
	if( bid != XI_ERR_NOBUTTON ) {				// press was not on a button do something else maybe?
		cbch_button_press( window, bid );
		return XI_OK;
	}

	e = (XButtonEvent *) event;
	cbch_mouse_press( window, e->x, e->y, e->button );
	return XI_OK;
}

*/
import "C"
