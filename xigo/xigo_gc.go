/*
	Mnemonic:	Xgo_cg
	Abstract:	Main gc function providing the sketch interface for programmes wishing
				to render their output into an X-window (via the XI library).

				Active pages/subpages in an X environment are a bit different than
				for page oriented sketch environments.  Pages are real (bordered)
				windows which can be sized and moved interactively by the window manager
				without any 'user' code.  Subpages are basically clipped regions, but
				in an X world they are child windows which have the ability to be
				double-buffered, and paint order is important.  The active page is the
				target for all drawing operations and may be a page (root window), or
				a subpage. To make a sub page active, the parent (root) page must be
				selected, then the subpage selected. 
				
	Date:		15 July 2018
	Author:		E. Scott Daniels
*/

package xigo


// ---------- C interface preamble -------------------------------------------------
/*
	Because we export functions in this module we canNOT add external functions
	to the C code below.  External functions that handle real mouse button presses,
	and are given as function pointers here, are defined in xgo_page_ext.go. Their 
	prototypes must be here.
*/

/*
#cgo CFLAGS: -I /usr/include -I /usr/include/freetype2 
#cgo LDFLAGS: -lxi -lXft -lfontconfig -lX11-xcb -lXext -lX11 -lm

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xdbe.h>

#include <xi/xi.h>

// ---- protos of callback wrapper functions -----
//int cb_mbutton_press( void* data, int display, int window, void* ev );
//int cb_mbutton_rel( void* data, int display, int window, void* ev );
//typedef int (*intFunc)();

*/
import "C"

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"unsafe"

	"gitlab.com/rouxware/goutils/clike"
	"bitbucket.org/EScottDaniels/colour"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

// CAUTION: there is no way to reference these from the 'parent' without managing this directory
//          at the same level as sketch. So, these are defined in all places, and user will reference
//          them as sketch.FILL_*. These must match the constants in sketch.
const (
    FILL_NONE int = iota
    FILL_SOLID
    FILL_OP_0           //  for X these aren't implemented (coudl we do an alpha channel thing?)
    FILL_OP_1
    FILL_OP_2
    FILL_OP_3
    FILL_OP_4
    FILL_OP_5
    FILL_OP_6
    FILL_HORIZ
    FILL_VERT
)

const (
	EV_PRESS int = iota				// event types
	EV_RELEASE
	EV_MOUSE_PRESS
	EV_MOUSE_REL
)

const (
	BT_RADIO	int = C.XI_RADIO_BUTTON				// button types
	BT_STICKY	int = C.XI_TOGGLE_BUTTON
	BT_SPRING	int = C.XI_SPRING_BUTTON
	BT_ENTRY	int = C.XI_ENTRY_BUTTON
)

type saved_state struct {
	active 		*Xigo_page						// active page or subpage which receives drawing requests
	active_font	C.int							// font currently set
	active_colour C.int							// index of last set colour
	active_fcolour C.int						// active fill colour
	fill_style	int								// current fill style
	line_dashed bool
	line_width	int
	scale_x		float64							// scale settings
	scale_y		float64
	orig_x		float64
	orig_y		float64
}

/*
	An interactive event. All fields are public as the event is written back to
	the user
type Ievent struct {
	Event	int									// EV_* constants
	Xgc		*Xigo_gc							// the related graphics context
	X		int									// x,y location of pointer (for some events)
	Y		int
	Data	int									// specific type data if needed (e.g. mouse button)
}
*/

type Xigo_gc struct {
	display 	C.int							// the display reference
	draw_lock	*sync.Mutex						// we must synchronise updates to the display
	pages 		map[string]*Xigo_page			// list of pages
	colour2idx	map[string]C.int				// colour name to clut index
	font2id		map[string]C.int				// colour name to clut index
	clut_idx 	C.int							// index where next add to clut should go

	scale_x		float64							// scale in x,y directions
	scale_y		float64							// right now only x value is used and we scale equally in each direction
	orig_x		float64							// translated origin coords
	orig_y		float64

	states		[]*saved_state					// saved states
	state_idx	int

	active 		*Xigo_page						// active page or subpage which receives drawing requests
	active_font	C.int							// font currently set
	active_colour C.int							// index of last set colour
	active_fcolour C.int						// active fill colour
	fill_style	int								// current fill style
	line_dashed	bool							// true if dashed lines are on
	line_width	int

	listener	chan *sktools.Interaction				// channel to write interactive events to
}

var ( 
	win2xgc	map[int] *Xigo_gc;			// allows us to map a window ID back to a gc without passing pointer data to XI
)

// ----------------- private support functions ------------------------------------------------------

/*
	scale2cint will scale the given Go integeger value  returning a C integer.
*/
func (xgc *Xigo_gc ) scale_cint( ivalue interface{} ) ( C.int ) {
	var (
		value float64
	)

	switch tv := ivalue.( type ) {
		case float64:
			value = tv

		case int:
			value = float64( tv )

		case int32:
			value = float64( tv )

		case int64:
			value = float64( tv )

		case *int:
			value = float64( *tv )

		return C.int( 0 )
			
	}

	return C.int( value * xgc.scale_x )
}

/*
	scale2int will scale the given Go integeger value  returning a go integer.
*/
func (xgc *Xigo_gc ) scale_int( ivalue interface{} ) ( int ) {
	var (
		value float64
	)

	switch tv := ivalue.( type ) {
		case float64:
			value = tv

		case int:
			value = float64( tv )

		case int32:
			value = float64( tv )

		case int64:
			value = float64( tv )

		case *int:
			value = float64( *tv )

		return 0
			
	}

	return int(  value * xgc.scale_x )
}

/*
	Block until we have the draw lock for the gc.
*/
func ( xgc *Xigo_gc ) Lock( ) {
	if xgc == nil {
		return
	}

	xgc.draw_lock.Lock()
}

/*
	Release the lock.
*/
func ( xgc *Xigo_gc ) Unlock( ) {
	if xgc == nil {
		return
	}
	xgc.draw_lock.Unlock()
}

/*
	Force the given colur value to be set in a window that is likely not the active window.
*/
func (xgc *Xigo_gc ) force_win_colour( window C.int, colour C.int ) { 
	C.XIsetcolour( window, colour )
}

/*
	Force the given colur value to be set. Must be called after 
	a window change.
*/
func (xgc *Xigo_gc ) force_colour( colour C.int ) { 
	xgc.active_colour = -1
	xgc.set_colour( colour )
}


/*
	X uses a colour lookup table (clut) so we manage colour 'values' in a map that translates
	names and/ or #rrggbb strings to indexes in the clut.  If we don't have an entry, we add 
	the colour to the lookup table and stash it in our map, then we set the colour on the display.

	icref is the name/value of the desired colour. If it's set, we do nothing
	otherwise we set it, defining it to X if needed. 

	Returns the colour index for the colour.

	CAUTION: We need to hold the draw lock before calling this funciton
*/
func ( xgc *Xigo_gc ) set_colour( icref interface{} ) ( C.int ) {
	var (
		cidx C.int
		cname string
	)

	if xgc == nil {
		return 0
	}

	window := xgc.active.Get_win()
	switch cref := icref.(type) {
		case int:

			if xgc.active_colour != C.int( cref ) {
				C.XIsetcolour( window, C.int( cref) )
				xgc.active_colour = C.int( cref )
			}
			return C.int( cref )

		case C.int:
			if xgc.active_colour != cref {
				C.XIsetcolour( window, cref )
				xgc.active_colour = cref
			}
			return cref 

		case string:
			cname = string( cref )
			cidx = xgc.colour2idx[cname]
			if cidx != 0  {									// undefined colour will be defined below
				if xgc.active_colour != cidx {				// set only if valid and not already active
					C.XIsetcolour( window, cidx )
					xgc.active_colour = C.int( cidx )
				}

				return cidx
			}

		default:
			return 0
	}

	r, g, b := colour.Mk_colour( cname ).Get_rgb()
	xgc.colour2idx[cname] = xgc.clut_idx

	C.XIdefcolour( window, xgc.clut_idx, C.int( r ), C.int( g ), C.int( b ) )
	C.XIsetcolour( window, xgc.clut_idx )
	xgc.clut_idx++

	xgc.active_colour = xgc.colour2idx[cname]
	return xgc.colour2idx[cname]
}

/*
	Similar to set_colour, only this just adds a colour to the lookup table.
	The index is returned in a form that XI functions that want an index
	can use.
*/
func ( xgc *Xigo_gc ) add_colour( cname string ) ( C.int ){
	if xgc == nil {
		return 0
	}

	cidx := xgc.colour2idx[cname]
	if cidx != 0 {
		return cidx
	}

	window := xgc.active.Get_win()
	r, g, b := colour.Mk_colour( cname ).Get_rgb()
	xgc.colour2idx[cname] = xgc.clut_idx

	C.XIdefcolour( window, xgc.clut_idx, C.int( r ), C.int( g ), C.int( b ) )	// define colur in X clut
	cidx = xgc.clut_idx
	xgc.clut_idx++

	return cidx
}

/*
	Get the colour index for the colour name or the rgb string (#rrggbb) passed in.
*/
func ( xgc *Xigo_gc ) get_cidx( cname string ) ( C.int ) {
	if xgc == nil {
		return 0
	}

	cidx := xgc.colour2idx[cname]
	if cidx > 0 {
		return cidx
	}

	return xgc.add_colour( cname )
}

/*
	Add_font performs the font translation and adds it to the font table returning
	a font_id which can be sued to reference the font when creating text things.
*/
func ( xgc *Xigo_gc ) add_font( fontname string, size int, style int ) ( C.int ){ 
	if xgc == nil {
		return 0
	}

	fstr := fmt.Sprintf( "%s-%d-%d", fontname, size, style )
	fid := xgc.font2id[fstr]
	if fid != 0 {
		return fid				// already known
	}

	
	window := xgc.active.Get_win()
	cfname := C.CString( fontname ) 
	defer C.free( unsafe.Pointer( cfname ) )

	fid = C.XIxft_find_font( window, cfname, C.int( size ), C.int( style ) )		// define it and return the id
	xgc.font2id[fstr] = fid

	return fid
}

// ------------------ public things ---------------------------------------------------------------------

/*
	Mk_xigo_gc will create the sketch api compatable graphic context for darawing to the x environment
	using the XI C "package".  The output parameter specifies the display and root window size and the 
	title as comma separated items. This is of the format:
		hostname:display.screen,height,width,title

	The hostname informtation may be omitted (e.g. ,height,width) and if so the contents 
	of the DISPLAY environment variable will be used. If either height or width is omitted
	the value of 640 is used.
*/
func Mk_xigo_gc( output string ) (  gc *Xigo_gc, err error ) {
	gc = &Xigo_gc {
		draw_lock:	new( sync.Mutex ),
		colour2idx: make( map[string]C.int, 37 ),
		font2id:	make( map[string]C.int, 37 ),
		pages:		make( map[string]*Xigo_page, 37 ),
		states:		make( []*saved_state, 128 ),
		clut_idx:	C.XI_LAST_PREDEF_COLOUR+1,				// starting point for our assigned colour indexes
		line_width:	1,
		line_dashed: false,
		scale_x:	1.0,
		scale_y:	1.0,
	}

	height := 640
	width := 640

	C.XIinit()									// ignition

	tokens := strings.SplitN( output, ",", 4 )
	if tokens[0] == "" {
		gc.display, _ = C.XIopend( nil )
	} else {
		dstring := C.CString( tokens[0] )
		defer C.free( unsafe.Pointer( dstring ) )
		gc.display, _ = C.XIopend( dstring )
	}

	if gc.display < 0 {
		return nil, fmt.Errorf( "cannot open display: %s", output )
	}

	if len( tokens ) > 1 && tokens[1] != "" {
		height = clike.Atoi( tokens[1] )
	}
	if len( tokens ) > 2 && tokens[2] != "" {
		width = clike.Atoi( tokens[2] )
	}

	title := ""
	if len( tokens ) > 3 {
		title = tokens[3]
	}

	err = gc.Mk_page( "root", height, width, title )		// open the root window
	if err != nil {
		gc = nil
	} 

	gc.Lock()
	gc.active_fcolour = gc.add_colour( "black" )		// these things need the window to be open before we can set them
	gc.active_colour = gc.set_colour( "white" )
	gc.Unlock()

	return gc, err
}

/*
	These functions must exist to fulfill the sketch interface:
*/

func( xgc *Xigo_gc ) Clear( ) {
	if xgc == nil {
		return
	}

	old_colour := xgc.active_colour
	old_active := xgc.active

	xgc.Lock()

	for pn, p := range xgc.pages {
		if pn != "default" {							// default doubles with their name, so skip
			p.Clear( xgc, true )						// clear page and any subpages.
		}
	}

	xgc.active = old_active 
	xgc.force_colour( old_colour )
	xgc.Unlock()
}

/*
	Clear_subpage clears the active page.
*/
func( xgc *Xigo_gc ) Clear_subpage( ccolour string ) {
	old_colour := xgc.active_colour
	xgc.Lock()

	xgc.active.Set_page_colour( xgc.add_colour( ccolour ) )
	xgc.active.Clear( xgc, false )						// forces page colour and clears just the active page
	xgc.set_colour( old_colour )

	xgc.Unlock()
}


/*
	Close overall root window and clean up everything.
*/
func( xgc *Xigo_gc ) Close( ) {
	if xgc == nil {
		return
	}
}

/*
	Delete_subpage deletes the named subpage. It attempts to find the named page
	as a subpage to the active page, and if that fails it looks for a subpage to the
	root page. 
*/
func( xgc *Xigo_gc ) Delete_subpage( pname string ) {
	var (
		p *Xigo_page
	)

	if xgc == nil {
		return
	}

	parent := xgc.active
	if parent != nil {
		p = xgc.active.pages[pname]
	}
	if p == nil {						// attempt to select named page from the active page's list
		parent = xgc.pages["root"]
		p = parent.pages[pname]
	}
	if p == nil {
		return
	}

	xgc.Lock()
	p.Close( )
	xgc.Unlock()
	delete( parent.pages, pname )
}

/*
	Draw_arc causes an arc to be drawn with the center at xo,yo starting at alpha1 degrees to
	alpha2 degrees with the given radius.  Degrees are measured anticlockwise with 0 being 
	the X-axis of the carteisan plane (3 o'clock).
*/
func( xgc *Xigo_gc ) Draw_arc( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64  ) {
	if xgc == nil {
		return
	}

	window := xgc.active.Get_win()
	sweep := alpha2 - alpha1			// alpha 2 in X functions is the sweep angle, not angle relative to 0

	xgc.Lock()
	C.XIdraw_arc( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.scale_cint( radius ), C.int( alpha1 ), C.int( sweep ), C.XI_OPT_BB )
	xgc.Unlock()
}

func( xgc *Xigo_gc ) Draw_circle( xo float64, yo float64, radius float64, outline bool ) {
	if xgc == nil {
		return
	}

	opts := C.int( C.XI_OPT_BB )
	xgc.Lock()
	defer xgc.Unlock()

	window := xgc.active.Get_win()

	if xgc.fill_style > FILL_NONE {
		old_act := xgc.active_colour
		xgc.set_colour( xgc.active_fcolour )
		C.XIdraw_circle( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.scale_cint( radius ), opts | C.XI_OPT_FILL )
		xgc.set_colour( old_act )
	}

	if outline {
		C.XIdraw_circle( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.scale_cint( radius ), opts )
	}
}

/*
	Draw_line will draw a line from x1,y1 to x2,2 using the currently set colour.
*/
func( xgc *Xigo_gc ) Draw_line( x1 float64, y1 float64, x2 float64, y2 float64 ) {
	if xgc == nil {
		return
	}

	window := xgc.active.Get_win()		// window of active page/subpage
	
	xgc.Lock()
	C.XIdrawl( window, xgc.scale_cint( x1 ), xgc.scale_cint( y1 ), xgc.scale_cint( x2 ), xgc.scale_cint( y2 ), C.XI_OPT_BB );
	xgc.Unlock()
}


/*
	Draw_pie will draw a 'filled' arc which is the shape of a pie.  The pie is drawn with a center (point)
	at xo,yo and radius. The start and end angles are defined by alpha1 and alpha2; angles are from the
	X-axis of the cartesian plane (3 o'clock) and increase anticlockwise.

	NOTE: X windows second angle is RELATIVE to the first, an thus is the number of degrees to 
		sweep and NOT the angle from 0.  The angle2 we accept for these functions IS the angle 
		from 0 so we must compute the sweep.
*/
func( xgc *Xigo_gc ) Draw_pie( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64, outline bool ) {
	if xgc == nil {
		return
	}

	window := xgc.active.Get_win()

	sweep := alpha2 - alpha1

	xgc.Lock()
	old_act := xgc.active_colour
	xgc.set_colour( xgc.active_fcolour )
	C.XIdraw_pie( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.scale_cint( radius ), C.int( alpha1 ), C.int( sweep ), C.XI_OPT_FILL | C.XI_OPT_BB )
	xgc.set_colour( old_act )
	xgc.Unlock()
}

/*
	Given a set of points, draw a polygon optionally filling it.
*/
func( xgc *Xigo_gc ) Draw_poly( points []*sktools.Point, outline bool ) {
	if xgc == nil {
		return
	}

	xpts := C.XIpt_alloc( C.int( len( points ) ) )
	if xpts == nil {
		return
	}

	for i := range points {
		x, y := points[i].Get_xy()
		C.XIpt_add( xpts, C.int( i ), xgc.scale_cint( x ), xgc.scale_cint( y ) )
	}

	window := xgc.active.Get_win()

	xgc.Lock()
	if xgc.fill_style > FILL_NONE {
		old_act := xgc.active_colour
		xgc.set_colour( xgc.active_fcolour )
		C.XIdrawp_voidp( window, xpts, C.int( len( points ) ), C.XI_OPT_FILL | C.XI_OPT_BB )
		xgc.set_colour( old_act )
	}

	if outline {
		C.XIdrawp_voidp( window, xpts, C.int( len( points ) ), C.XI_OPT_BB )
	}
	xgc.Unlock()

	C.XIpt_free( xpts )
}

/*
	Draw_rect will draw a rectangle with its lower left corner at xo,yo. 
*/
func( xgc *Xigo_gc ) Draw_rect( xo float64, yo float64, height float64, width float64, outline bool ) {
	if xgc == nil {
		return
	}

	opts := C.int( C.XI_OPT_BB )
	xgc.Lock()
	defer xgc.Unlock()

	window := xgc.active.Get_win()

	if xgc.fill_style > FILL_NONE {
		old_act := xgc.active_colour
		xgc.set_colour( xgc.active_fcolour )
		C.XIdrawr( window, xgc.scale_cint( xo ), xgc.scale_cint( yo), xgc.scale_cint( height ), xgc.scale_cint( width ), opts | C.XI_OPT_FILL )
		xgc.set_colour( old_act )
	}

	if outline {
		C.XIdrawr( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.scale_cint( height ), xgc.scale_cint( width ), opts )
	}
}

/*
	Draw_text will cause the given string to be drawn at xo,yo in the active page/subpage.
*/
func( xgc *Xigo_gc ) Draw_text( xo float64, yo float64, text string  ) {
	if xgc == nil {
		return
	}

	window := xgc.active.Get_win()
	xgc.Lock()
	ctext := C.CString( text )
	defer C.free( unsafe.Pointer( ctext ) )
	C.XIxft_draw_text( window, xgc.scale_cint( xo ), xgc.scale_cint( yo ), xgc.active_colour, xgc.active_font, ctext, C.XI_OPT_BB )
	xgc.Unlock()
}

func( xgc *Xigo_gc ) Engague( ) {
	if xgc == nil {
		return
	}
}

/*
	Mk_subpage creates a subpage (a child window in X terms) at the given ulx, uly
	position with the requested height and width.
*/
func( xgc *Xigo_gc ) Mk_subpage( pname string, ulx float64, uly float64, height float64, width float64 ) {
	if xgc == nil {
		return
	}

	// do NOT scale values -- the drawing process of a subPage will scale then
	xgc.active.Add_subpage( pname, int( ulx ), int( uly ), int( height ), int( width ) )
}

/*
	Mk_locked_subpage creates a subpage which is immune to scale changes, and the initial size and placement is NOT
	affected by the current scale.
*/
func( xgc *Xigo_gc ) Mk_locked_subpage( pname string, xo float64, yo float64, height float64, width float64 ) {
	if xgc == nil {
		return
	}

	sp := xgc.active.Add_subpage( pname, int( xo ), int( yo ), int( height ), int( width ) )
	sp.Lock()
}

/*
	Pop_state pops updates various things in the context from the last pushed state.
*/
func( xgc *Xigo_gc ) Pop_state( ) {
	if xgc == nil {
		return
	}
	
	if xgc.state_idx > 0 {
		xgc.state_idx--
		st := xgc.states[xgc.state_idx]
		if st != nil {
			xgc.active = st.active
			xgc.active_font = st.active_font
			xgc.active_colour = st.active_colour
			xgc.active_fcolour = st.active_fcolour
			xgc.fill_style = st.fill_style

			if st.scale_x != xgc.scale_x || st.scale_y != xgc.scale_y {			// these things need to be 'pushed' down into XI as they affect painting
				xgc.scale_x = st.scale_x
				xgc.scale_y = st.scale_y
				C.XIscale( xgc.active.Get_win(), C.double( xgc.scale_x ) )
			}

			if xgc.orig_x != st.orig_x || xgc.orig_y != st.orig_y {		// only need to reset if changed
				xgc.orig_x = st.orig_x
				xgc.orig_y = st.orig_y

				xgc.Translate( xgc.orig_x, xgc.orig_y )
			}

			if st.line_width != xgc.line_width || st.line_dashed != xgc.line_dashed {
				dashed := 0
				if st.line_dashed {
					dashed = 1
				}
				C.XIline_attrs( xgc.active.Get_win(), C.int( dashed ), C.int( st.line_width ), 0, 0 )
			}

			xgc.line_width = st.line_width
			xgc.line_dashed = st.line_dashed
		}

		xgc.states[xgc.state_idx] = nil
	}
}

/*
	Push_state saves various things from the current state.
*/
func( xgc *Xigo_gc ) Push_state( ) {
	if xgc == nil {
		return
	}

	if xgc.state_idx < len( xgc.states ) {
		st := &saved_state {
			active:				xgc.active,
			active_font: 		xgc.active_font,
			active_colour: 		xgc.active_colour,
			active_fcolour: 	xgc.active_fcolour,
			fill_style: 		xgc.fill_style,
			line_dashed:		xgc.line_dashed,
			line_width:			xgc.line_width,
			scale_x:			xgc.scale_x,
			scale_y:			xgc.scale_y,
			orig_x: 			xgc.orig_x,
			orig_y: 			xgc.orig_y,
		}

		xgc.states[xgc.state_idx] = st
		xgc.state_idx++
	}
}


/*
	Rotate will logically rotate the page by the specified degrees.
	Currently not supported for XI, so we return false to indicate
	that rotation 'failed'.
*/
func( xgc *Xigo_gc ) Rotate( degrees float64 ) ( bool ) {
	if xgc == nil {
		return false
	}

	return false
}

/*
	Set the visibility for a subpage.
*/
func ( xgc *Xigo_gc ) Set_visibility( pname string, hidden bool ) {
	if xgc == nil {
		return
	}

	p := xgc.active.Get_subpage( pname )
	if p == nil {
		p = xgc.pages["root"].Get_subpage( pname )
	}
	if p != nil {
		if hidden {
			p.Set_invisible()
		} else {
			p.Set_visible()
		}
	} else {
		fmt.Fprintf( os.Stderr, "set vis: couldn't find page in active or in root: %s active=%s\n", pname, xgc.active.name )
	}
}

/*
	Select_page makes the named page the active page which becomes the target for all subsequent
	drawing operations. Unlike the select_subpage, this function always looks at the top level
	window (page) and ignores the currently active page.
*/
func( xgc *Xigo_gc ) Select_page( pname string ) {
	if xgc == nil {
		return
	}

	p := xgc.pages["root"]
	if pname != "root" {
		p = p.pages[pname]
	} 

	if p != nil {
		xgc.active = p
		xgc.force_colour( xgc.active_colour )
		p.Set_colour( xgc.active_colour )
		return
	} else {
		fmt.Fprintf( os.Stderr, "select: couldn't find page: %s\n", pname )
	}
}

/*
	Select_subpage selects the named subpage for subsequent drawing actions. The active
	page is searched first, and if the named page isn't there, then the default page is
	searched. The special names "root" or "default" may be used to select the root window.

	If the named page was found we return true.
*/
func( xgc *Xigo_gc ) Select_subpage( pname string ) ( found bool ){
	if xgc == nil {
		return false
	}

	if pname == "default" || pname == "root" {
		xgc.active = xgc.pages[pname]
		return true
	}

	if xgc.active == nil {					// shouldn't be nil, but if it is select default
		xgc.Select_page( "default" )
		if xgc.active == nil  {
			return true
		}
	}

	ap := xgc.active
	p := ap.pages[pname]					// search the active page for name
	if p == nil {
		p = xgc.pages["root"].Get_subpage( pname )	// and search in root if not in active
	}

	if p != nil {
		xgc.active = p
		xgc.force_colour( xgc.active_colour )
		return true
	}

	return false
}

/*
	Set_fill_sttrs allows the caller to control the fill attribute (fill/unfilled)
	for the shape drawing funcitons.
*/
func( xgc *Xigo_gc ) Set_fill_attrs( cname string, fill_style int ) {
	if xgc == nil {
		return
	}

	xgc.active_fcolour = xgc.add_colour( cname )
	xgc.fill_style = fill_style
}

/*
	Set_font finds and loads the indicated font.  Weights < 2 are considered 'normal';
	>=2 are 'bold'. 
*/
func( xgc *Xigo_gc ) Set_font( fontname string, points int, weight int, ital bool ) {
	if xgc == nil {
		return
	}

	style := 0
	if weight > 1 {
		style |= C.XI_FS_BOLD
	}
	if ital {
		style |= C.XI_FS_ITAL
	}
	
	xgc.Lock()
	fid := xgc.add_font( fontname, xgc.scale_int( points ), style )			// look up font id, add to font tables if needed
	if xgc.active_font != fid {
		window := xgc.active.Get_win()
		C.XIxft_use_font( window, fid )
		xgc.active_font = fid
	}
	xgc.Unlock()
}

/*
	Set_colour accepts a colour value string (e.g. #rrggbb) or recognised name (e.g. "white")
	and makes that the active colour for subsequent drawing options.
*/
func( xgc *Xigo_gc ) Set_colour( cname string ) {
	if xgc == nil {
		return
	}

	xgc.Lock()
	xgc.set_colour( cname )
	xgc.Unlock()

	xgc.active.Set_colour( xgc.active_colour )		// the page must track active too
}

/*
	Set diminsions is ignored. A window should be sizable by the user. 
*/
func( xgc *Xigo_gc ) Set_dimensions( height float64, width float64 ) {
	if xgc == nil {
		return
	}
}

/*
	Set_line_style configures the rendering for lines until changed.

	This is fashioned after the postscript line style mechanism that takes an on off
	set of integers (e.g. 3 on, 2 off, or 3 on 2 off, 4 on 2 off, etc.). To set a 
	solid line in postscript, a nil pointer is passed. X doesn't support such a rich 
	notaion of line styles so the interpretation here is:
		nil -- solid line
		!nil - dashed line
*/
func( xgc *Xigo_gc ) Set_line_style( pattern []int ) {
	if xgc == nil {
		return
	}


	dashed := 0
	if pattern == nil {
		xgc.line_dashed = false
	} else {
		dashed = 1
		xgc.line_dashed = true
	}

	C.XIline_attrs( xgc.active.Get_win(), C.int( dashed ), C.int( xgc.line_width ), 0, 0 )
}

/*
	Set the line wdith to desired size. setting to 0 causes the smallest possible
	(fastest) line to be drawn.
*/
func( xgc *Xigo_gc ) Set_line_width( width int ) {
	if xgc == nil {
		return
	}

	if width < 0 {
		width = 0
	}

	xgc.line_width = width

	dashed := 0				// convert to integer for x call
	if xgc.line_dashed {
		dashed = 1
	}

	C.XIline_attrs( xgc.active.Get_win(), C.int( dashed ), C.int( xgc.line_width ), 0, 0 )
}

func( xgc *Xigo_gc ) Set_page_colour( cname string ) {
	if xgc == nil {
		return
	}

	xgc.active.Set_page_colour( xgc.add_colour( cname ) )
}

/*
	Set_scale captures the new desired scale for things drawn in the pages known to 
	this graphics context. For the most part, only xscale is applied since the 
	underlying XI lib doesn't support independent scale values as postscript does, 
	but we must accept same parms as the postscript functions.
*/
func( xgc *Xigo_gc ) Set_scale( xscale float64, yscale float64 ) {
	if xgc == nil {
		return
	}

	xgc.scale_x = xscale
	xgc.scale_y = yscale

	C.XIscale( xgc.active.Get_win(), C.double( xgc.scale_x ) )

	for _, sp := range xgc.pages {
		sp.Scale_subpages( xscale, true )		// scale all subpages to this page
	}
}

/*
	Set_iscale increments the scale by the value passed in, the increment is applied
	to both x and y scales.  For the most part, only xscale is applied since the 
	underlying XI lib doesn't support independent scale values as postscript does, 
	but we must accept same parms as the postscript functions.
*/
func( xgc *Xigo_gc ) Set_iscale( increment float64 ) {
	if xgc == nil {
		return
	}

	xgc.scale_x += increment
	xgc.scale_y += increment

	C.XIscale( xgc.active.Get_win(), C.double( xgc.scale_x ) )

	for _, sp := range xgc.pages {
		sp.Scale_subpages( xgc.scale_x, true )		// scale all subpages to this page
	}
}

/*
	Show causes all pages to be updated (shown). This is done by drawing each page
	which in turn draws each subpage.
*/
func( xgc *Xigo_gc ) Show( ) {
	if xgc == nil {
		return
	}

	for pn, p := range xgc.pages {
		if pn != "default" {
			xgc.Lock()
			p.Show( )
			xgc.Unlock()
		}
	}
}

/*
	Translate applies the hard x,y value as the coordinates of the page's origin
	for the active page.  Current setting are saved so that push/pop can restore
	the setting.
*/
func( xgc *Xigo_gc ) Translate(  x float64, y float64 ) {
	if xgc == nil {
		return
	}

	xgc.orig_x = x
	xgc.orig_y = y
	C.XIxlate( xgc.active.Get_win(), C.int( x ), C.int( y  ) )

	for _, sp := range xgc.pages {							// must move subpage windows manually
		sp.Move_subpages( int( x ), int( y ), xgc.scale_x, true )		// move all subpages to this page
	}
}

/*
	Translate_delta shifts the page origin by deltax,deltay for the active page.
*/
func( xgc *Xigo_gc ) Translate_delta(  xdelta float64, ydelta float64 ) {
	if xgc == nil {
		return
	}

	xgc.orig_x += xdelta
	xgc.orig_y += ydelta
	C.XIdelta_xlate( xgc.active.Get_win(), C.int( xdelta ), C.int( ydelta ) )

	for _, sp := range xgc.pages {							// must move subpage windows manually
		sp.Move_subpages( int( xgc.orig_x ), int( xgc.orig_y ), xgc.scale_x, true )		// move all subpages to this page
	}
}

