// :vi ts=4 sw=4 noet :

/*
	Mnemonic:	font.go
	Abstract:	Manage fonts to avoid the typically costly allocation of fonts as a user
				application may shift between them willy nilly.
	Author:		E. Scott Daniels
	Date:		29 April 2018
*/

package qt

import(
	"github.com/therecipe/qt/gui"
)

type Font_mgr struct {
	loaded	map[string]*Font
}

type Font struct {
	name string
	size int
	weight int
	ital bool
	f *gui.QFont	// the actual font
}

/*
	Allocate a font manager.
*/
func Mk_font_mgr( ) ( *Font_mgr ) {
	fm := &Font_mgr {
		loaded: make( map[string]*Font, 17 ),
	}

	return fm
}

/*
	Create a font and save it with a user supplied reference name.
*/
func ( fm *Font_mgr ) Add( ref_name string, name string, size int, weight int, ital bool ) ( *Font ) {
	if fm == nil {
		return nil
	}

	font := &Font {
		name: name,
		size: size,
		weight: weight,
		ital:	ital,
	}

	font.f =  gui.NewQFont2( name, size, weight, ital )

	fm.loaded[ref_name] = font

	return font
}

/*
	Look up and return the font using the reference name. The font returned
	is suitable for passing to QT functions.
*/
func ( fm *Font_mgr ) Get( ref_name string ) ( *gui.QFont ) {
	if fm == nil {
		return nil
	}

	ent := fm.loaded[ref_name]
	if ent == nil {
		return fm.loaded["default"].f
	}

	return ent.f
}


/*
	Given a set of specs, return true if this font is the same
func ( font *Qt_font ) Is_font( name string, size int, weight int, ital bool ) ( bool ) {
	if font != nil {
		if font.weight == weight && font.size == size  && font.ital == ital && font.name == name {
			return true
		}
	}

	return false
}
*/
