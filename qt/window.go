// :vi ts=4 sw=4 noet :

/*
	Mnemonic:	font.go
	Abstract:	Manage fonts to avoid the typically costly allocation of fonts as a user
				application may shift between them willy nilly.
	Author:		E. Scott Daniels
	Date:		29 April 2018
*/

package qt

import (
	"github.com/therecipe/qt/widgets"
)


/*
	Manages the main window.
*/
type Qt_window struct {
	name string
	h		int
	w		int
	drawports map[string]*Qt_drawport
	active	*Qt_drawport
	main_window		*widgets.QMainWindow
}

func Mk_window( title string, h int, w int ) ( *Qt_window ) {
	win := &Qt_window {
		h: h,
		w: w,
		drawports: make( map[string]*Qt_drawport, 17 ),
	}

	win.main_window = widgets.NewQMainWindow( nil, 0 )		// what parameters can/should be passed here?

	if title != "" {
		win.main_window.SetWindowTitle( title )
	} else {
		win.main_window.SetWindowTitle( "no titile given" )
	}

	win.main_window.SetMinimumSize2( w, h )

	return win
}

/*
	Add a drawport to the window with origin ox,oy.
*/
func ( win *Qt_window ) Add_drawport(  name string, ox int, oy int, h int, w int ) {
	if win == nil {
		return
	}

	dp := Mk_drawport( ox, oy,  h, w )
	if dp != nil {
		dp.Set_colour( "black" )					// set default colour
		win.drawports[name] = dp	

		if win.active == nil {					// first added becomes active and default
			win.active = dp
			win.drawports["default"] = dp
			win.main_window.SetCentralWidget( dp.Get_object() )
		}
	}
}

/*
	Add a font to the active drawport 
	Future: add to all drawports
*/
func ( win *Qt_window ) Add_font(  ref_name string, font_name string, size int, weight int, ital bool ) {
	if win == nil {
		return
	}

	win.active.Add_font( ref_name, font_name, size, weight, ital )
}


/*
	Set the bg colour for the active drawport. Name is any valid colour name or hex rgb string. Mode
	is one of the fill mode constants
*/
func ( win *Qt_window ) Set_dp_colour(  cname string, mode int ) {
	if win == nil {
		return
	}

	colour := Mk_gen_colour( cname, 255 )
	win.active.Set_bg( colour, mode )
}


/*
	Sel_drawport selects the named drawport, making it active, and returns a pointer so that it
	can be used directly if desired.
*/
func ( win *Qt_window ) Sel_drawport(  name string ) ( dp *Qt_drawport ) {
	if win == nil {
		return
	}

	dp := win.drawports[name]
	if dp != nil {
		win.active = dp
	}

	return win.active
}

/*
	Paint all drawports in the window	
*/
func ( win *Qt_window ) Paint_all(  ) {
	if win == nil {
		return
	}

	win.active.Paint( )		// render and paint the active drawport
	win.main_window.Show()
}


func ( win *Qt_window ) Draw_line(  x1 float64, y1 float64, x2 float64,  y2 float64 ) {
	if win == nil {
		return
	}

	win.active.Draw_line( x1, y1, x2, y2, nil )
}


func ( win *Qt_window ) Draw_text(  x float64, y float64, text string, font string, colour string ) {
	if win == nil {
		return
	}

	win.active.Draw_text( x, y, text, font, colour )
}


func ( win *Qt_window ) Set_colour(  name string ) {
	if win == nil {
		return
	}

	win.active.Set_colour( name )
}


/*
	Set fill colour and style.
*/
func ( win *Qt_window ) Set_fill(  name string, style int ) {
	if win == nil {
		return
	}

	colour := Mk_gen_colour( name, 255 )
	win.active.Set_fill( colour, style )
}


