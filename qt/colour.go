// :vi ts=4 sw=4 noet :

/*
	Mnemonic:	colour.go
	Abstract:	Wrangle all of QT's colour flavours into a single construct.
	Author:		E. Scott Daniels
	Date:		29 April 2018
*/

package qt

import (
	clu "bitbucket.org/EScottDaniels/colour"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
)

/*
	QT seems to have mutiple ways to refernece colour and none are compatable
	with each other.  Damn C++ programmers. So, we manage a generic colour
	struct which can be used to track a colour in various shapes and sizes
	to be passed to QT in what ever fashion it fancies. 
*/
type Gen_colour struct {
	gui_colour	*gui.QColor				// the gui representation
	int_colour	core.Qt__GlobalColor	// the integer representation
	cinfo		*clu.Colour				// our colour maager representation
	r			int						// sane colour 0-255
	g			int
	b			int
	alpha		int
}

/*
	Given a string (#rrggbb, 0xrrggbb or name)
*/
func Mk_gen_colour( cvalue string, alpha int ) ( *Gen_colour ) {
	gc := &Gen_colour{
		cinfo: clu.Mk_colour( cvalue ),
		alpha:	alpha,
	}

	gc.r, gc.g, gc.b = gc.cinfo.Get_rgb( )
	gc.gui_colour = gui.NewQColor3( gc.r, gc.g, gc.b, gc.alpha )
	gc.int_colour = core.Qt__GlobalColor( gc.cinfo.Get_composite( ) )

	return gc
}

/*
	Return the equiv qt colour object.
*/
func ( gc *Gen_colour ) Get_gui_colour( ) ( *gui.QColor ) {
	if gc == nil {
		return nil
	}

	return gc.gui_colour
}

/*
	Return the equiv qt core global colour object.
*/
func ( gc *Gen_colour ) Get_global_colour( ) ( core.Qt__GlobalColor ) {
	if gc == nil {
		return core.Qt__GlobalColor( 0 )
	}

	return gc.int_colour
}
