
/*
	Abstract:	This provides the sketch api using qt as the underlying drawing/rendering
				component making it (qt) simpler to use.
	Date:		1 April 2018
	Author:		E. Scott Daniels
*/

package qt

import (
	"fmt"

	// doc: https://godoc.org/github.com/therecipe/qt
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/gui"

	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

// -----------------------------------------------------------------------------

const( 
	FILL_NONE	int = iota
	FILL_SOLID	
	FILL_OP_0	
	FILL_OP_1	
	FILL_OP_2	
	FILL_OP_3	
	FILL_OP_4	
	FILL_OP_5	
	FILL_OP_6	
	FILL_HORIZ
	FILL_VERT
)

// -------------- structs ----------------------------------------------------------

/*
	The qt context struct passed round bwtween user and functions here.
*/
type Qt_api struct {
	app *widgets.QApplication					// qt, sadly, is all encompassing
	windows map[string]*Qt_window
	font_ref	string							// active font reference name
	font	string								// active font info
	fsize	string
	colour	string	
	active	*Qt_window
	adp		*Qt_drawport						// drawport for direct painting
}

// ---- private -------------------------------------------------------------------

/*
	Generate a new pen based on colour, or return the default. 
*/
func ( dp *Qt_drawport ) get_pen( colour *Gen_colour ) ( pen *gui.QPen ) {

	if colour == nil {
		pen = dp.stylus
	} else {
    	pen = gui.NewQPen3( colour.Get_gui_colour( ) )
	}

	return pen
}

/*
	Set the drawport's fill colour and style (mode).  The mode is used to index into
	one of qt's preset styles. 
*/
func ( dp *Qt_drawport ) get_fill( colour *Gen_colour, mode int  ) ( fill *gui.QBrush ) {
	var (
		pat core.Qt__BrushStyle
	)

	switch( mode ) {
		case 0:
			pat = core.Qt__NoBrush         
    	case 1:
			pat = core.Qt__SolidPattern           
    	case 2:
			pat = core.Qt__Dense1Pattern          
    	case 3:
			pat = core.Qt__Dense2Pattern          
    	case 4:
			pat = core.Qt__Dense3Pattern          
    	case 5:
			pat = core.Qt__Dense4Pattern          
    	case 6:
			pat = core.Qt__Dense5Pattern          
    	case 7:
			pat = core.Qt__Dense6Pattern          
    	case 8:
			pat = core.Qt__Dense7Pattern          
    	case 9:
			pat = core.Qt__HorPattern             
    	case 10:
			pat = core.Qt__VerPattern             
    	case 11:
			pat = core.Qt__CrossPattern           
    	case 12:
			pat = core.Qt__BDiagPattern           
    	case 13:
			pat = core.Qt__FDiagPattern           
    	case 14:
			pat = core.Qt__DiagCrossPattern       
    	case 15:
			pat = core.Qt__LinearGradientPattern  
    	case 16:
			pat = core.Qt__RadialGradientPattern  
    	case 17:
			pat = core.Qt__ConicalGradientPattern 
    	case 18:
			pat = core.Qt__TexturePattern         
		default:
			pat = core.Qt__NoBrush         
	}


	if colour == nil {
		colour = dp.fcolour
	}

	return gui.NewQBrush3( colour.Get_gui_colour(), pat )
}


/*
	Create a graphics context into the QT world.
*/
func Mk_qt_gc(  ) ( gc *Qt_api ) {
	gc = &Qt_api { }
	gc.app = widgets.NewQApplication(  0, nil )
	gc.windows = make( map[string]*Qt_window, 7 )

	return gc
}

/*
	Add a new 'page' (window) with the given height, width and title. Returns error
	if unable. Name is the reference string that is used to select or address the window. 
*/
func ( qt *Qt_api ) Mk_page( name string, h int, w int, title string ) ( error ) {
	if qt == nil {
		return fmt.Errorf( "mk_page: qt api structure pointer was nil" )
	}

	if name == "" {
		return fmt.Errorf( "mk_page: page must be named" )
	}

	win := Mk_window( title, h, w )

	qt.windows[name] = win
	if qt.active == nil {				// first, make it the active window
		qt.active = win 
	}

	return nil
}

/*
	Set the active window for operations.
*/
func ( qt *Qt_api ) Select_page( name string ) {
	if qt == nil {
		return
	}

	w := qt.windows[name]
	if w != nil {
		qt.active = w
	}	
}

/*
	Add a drawport to the active window.
*/
func ( qt *Qt_api ) Mk_subpage( name string, ox float64, oy float64, h float64, w float64 ) {
	if qt == nil {
		return
	}

	qt.active.Add_drawport( name, int( ox ),  int( oy ),  int( h ),  int( w ) )
}

/*
	Define a font and add it to the active drawport.
	Future: add to all drawports
*/
func ( qt *Qt_api ) Set_font(  font_name string, size int, weight int, ital bool ) {
	if qt == nil {
		return
	}

	qt.font_ref = fmt.Sprintf( "%s-%d-%d-%v", font_name, size, weight, ital )
	qt.active.Add_font( qt.font_ref, font_name, size, weight, ital )
}

/*
	Select the named drawport in the active window. All drawing commands following the
	selection, until another is selected, are applied to this drawport.
*/
func ( qt *Qt_api ) Selelect_sub_page( name string ) {
	if qt == nil {
		return
	}

	qt.adp = qt.active.Sel_drawport( name )
}

/*
	Draw_arc draws an arc centered at x,y with the given readius.  Angles a1 and a2 are the
	start and ending points of the arc measured in degrees anticlokwise from the x-axis.
*/
func ( qt *Qt_api ) Draw_arc( x float64, y float64, radius float64, a1 float64, a2 float64 ) {
	if qt == nil || qt.adp == nil  {
		return
	}

	qt.adp.Draw_arc( x, y, radius, a1, a2 )
}

/*
	Draw a line in the active window/drawport.
*/
func ( qt *Qt_api ) Draw_line( x1 float64, y1 float64, x2 float64, y2 float64 ) {
	if qt == nil || qt.adp == nil  {
		return
	}

	qt.adp.Draw_line( x1, y1, x2, y2, nil )		// draw line in active window's active drawport
}

func ( qt *Qt_api ) Draw_text(xo float64, yo float64, text string) {
	if qt == nil || qt.adp == nil  {
		return
	}

	qt.adp.Draw_text(  xo, yo, text, qt.font_ref, "" )  // empty colour name draws in last set colour
}

/*
	Set the foreground colour for the current window's ative drawport
*/
func ( qt *Qt_api ) Set_colour( name string ) {
	if qt == nil {
		return
	}

	qt.active.Set_colour( name )
}

/*
func ( qt *Qt_api ) Set_fcolour( cname string ) {
	if qt == nil {
		return
	}

	qt.active.Set_fcolour( cname )
}
*/

/*
	Set the colour and fill style to use when filling objects.
*/
func (qt *Qt_api ) Set_fill_attrs( cname string, fill_style int ) {
	if qt == nil {
		return
	}

	qt.active.Set_fill( cname, fill_style )
}

/*
	Set the background colour for the active drawport in the active window. Cname is any valid
	colour name or hex rgb string.  Mode is any of the FILL_ mode constants.
*/
func ( qt *Qt_api ) Set_page_colour( cname string ) {
	if qt == nil {
		return
	}

	qt.active.Set_dp_colour( cname, FILL_SOLID )
}


/*
	Paint all windows.
*/
func ( qt *Qt_api ) Paint_all( ) {
	if qt == nil {
		return
	}
	
	qt.active.Paint_all( )
}



/*
	Pass control to the QT driver; all remaining interaction once the user calls this
	Function is through call backs.
*/
func ( qt *Qt_api ) Engague(  ) {
	if qt == nil {
		return
	}

	 widgets.QApplication_Exec()
}

/*
	These funcitons must exist, if only as dummies, to implement as sketch interface.
	The following are unimplemented or will always be dummies as they really aren't supported.
*/
func ( qt *Qt_api ) Clear() {
}
func ( qt *Qt_api ) Clear_subpage(pname string) {
}
func ( qt *Qt_api ) Close( ) {
}
func ( qt *Qt_api ) Delete_subpage(pname string) {
}
func ( qt *Qt_api ) Draw_circle( xo float64, yo float64, radius float64, outline bool ) {
}
func ( qt *Qt_api ) Draw_poly( point []*sktools.Point, outline bool ) {
}
func ( qt *Qt_api ) Draw_rect(xo float64, yo float64, height float64, width float64, outline bool ) {
}
func ( qt *Qt_api ) Draw_pie( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64, outline bool ) {
}
func ( qt *Qt_api ) Pop_state( ) {
}
func ( qt *Qt_api ) Push_state( ) {
}
func ( qt *Qt_api ) Rotate( degrees float64 ) {
}
func ( qt *Qt_api ) Select_subpage( pname string ) {
}
func ( qt *Qt_api ) Set_dimensions( height float64, width float64 ) {
}
func ( qt *Qt_api ) Set_line_style( pattern []int ) {
}
func ( qt *Qt_api ) Set_line_width( width int ) {
}
func ( qt *Qt_api ) Set_scale( xscale float64, yscale float64) {
}
func ( qt *Qt_api ) Show(  ) {
	if qt == nil {
		return
	}

	qt.Paint_all()
}
func ( qt *Qt_api ) Translate_delta( dx float64, dy float64 ) {
}

