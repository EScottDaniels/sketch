// :vi ts=4 sw=4 noet :

/*
	Mnemonic:	drawport.go
	Abstract:	Manages a clipped drawing area in the window.
	Author:		E. Scott Daniels
	Date:		29 April 2018
*/

package qt

import (
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/gui"
)

/*
	A portion of the window that can be drawn into independently forom other
	drawports.
*/
type Qt_drawport struct {
	ox	int		// origin (x,y) (lower left)
	oy	int
	h	int		// height/width
	w	int
	colour	*Gen_colour				// pen/text colour
	fcolour	*Gen_colour				// thing fill colour
	stylus	*gui.QPen				// current drawing pen/stylus
	fill	gui.QBrush_ITF			// fill parms for things
	bg		*Gen_colour				// drawport background colour
	dpfill	gui.QBrush_ITF			// fill parms for drawport background (page colour)
	canvas	*widgets.QWidget 
	scene	*widgets.QGraphicsScene
	view	*widgets.QGraphicsView
	fonts *Font_mgr
}

// --------------------------------------------------------------------------

func Mk_drawport( ox int, oy int, h int, w int ) ( *Qt_drawport ) {
	dp := &Qt_drawport {
		ox: ox,
		oy: oy,
		h:  h,
		w:  w,

    	canvas: widgets.NewQWidget( nil, 0 ),
    	scene: widgets.NewQGraphicsScene( nil ),
    	view: widgets.NewQGraphicsView( nil ),

		colour: Mk_gen_colour( "#7f0000", 255 ),		//gui.NewQColor3( 100, 0, 0, 255 ),
		fcolour: Mk_gen_colour( "#7f007f", 255 ),		//gui.NewQColor3( 100, 0, 100, 255 ),	// default fill colour
		bg: Mk_gen_colour( "#0a0a0a,", 255 ),		//gui.NewQColor3( 10, 10, 10, 255 ),			// drawport colour
	}

	dp.get_fill( dp.fcolour, FILL_SOLID )
    dp.view.SetScene( dp.scene )

    //layout := widgets.NewQVBoxLayout()		// added at render time for now. 
    //layout.AddWidget( dp.view, 0, 0 )

    //dp.canvas.SetLayout( layout )
	dp.canvas.SetBaseSize2( w, h )

	return dp
}

/*
	Returns the "outermost" QT object that is represnted by the drawport.
*/
func ( dp *Qt_drawport ) Get_object( ) ( *widgets.QWidget ) {
	if dp == nil {
		return nil 
	}

	return dp.canvas
}

/*
	Set the background colour for the drawport. Mode is one of the 
	FILL_* constants.
*/
func ( dp *Qt_drawport ) Set_bg( colour *Gen_colour, style int ) {
	if dp == nil {
		return 
	}

	if colour != nil {
		dp.bg = colour
	}

	if style >= 0 {
		dp.dpfill = dp.get_fill( dp.bg, style )
	}

	hold := dp.fill
	dp.fill = dp.dpfill
	dp.Draw_rect( 0, 0, float64( dp.h ), float64( dp.w ), dp.bg )

	dp.fill = hold
}

/*
	Set the fill (colour and style) for things that are filled (rectangle etc)
*/
func ( dp *Qt_drawport ) Set_fill( colour *Gen_colour, style int ) {
	if dp == nil {
		return 
	}

	dp.bg = colour
	dp.fill = dp.get_fill( dp.colour, style )
}

/*
	Set the drawing stylus:
		colour (r,g,b)
*/
func ( dp *Qt_drawport ) Set_stylus( ) {
	if dp == nil {
		return
	}

	colour := dp.colour
	if colour == nil {
		colour = Mk_gen_colour( "#7f7f7f", 255 )
	}

	if colour != nil {
    	dp.stylus = gui.NewQPen3( colour.Get_gui_colour() )
	}
}

/*
	Define a font and add it to the drawport
*/
func ( dp *Qt_drawport ) Add_font( ref_name string, name string, size int, weight int, ital bool ) {
	if dp == nil {
		return
	}

	if dp.fonts == nil {
		dp.fonts = Mk_font_mgr( )
		dp.fonts.Add( "default", name, size, weight, ital )			// first font is used as default
	}

	dp.fonts.Add( ref_name, name, size, weight, ital )
}

/*
func (ptr *QGraphicsView) Rotate(angle float64) {
func (ptr *QGraphicsView) Translate(dx float64, dy float64) {
*/

/*
	Draw_arc paintes an arc centered at xo,yo with the given radius.  The angles alpha1 and alpha2 
	define the starting (a1) and ending (a2) points in degrees anticlockwise from the X-axis.
*/
func ( dp *Qt_drawport ) Draw_arc( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64 ) {
	var( 
		colour	*Gen_colour
	)

	colour = dp.colour
	pen := dp.get_pen( colour )
    dp.scene.Addeplise2(  x1, y1, radius, radius, pen, dp.fill )
//func (ptr *QGraphicsScene) AddEllipse2(x float64, y float64, w float64, h float64, pen gui.QPen_ITF, brush gui.QBrush_ITF) *QGraphicsEllipseItem 

//func (ptr *QPainter) DrawArc3(x int, y int, width int, height int, startAngle int, spanAngle int) 

}


/*
	Add the text string with value to the canvas at the current position. Font is the reference name
	that was added to the canvas. The cname var may be a recognised name, or #rrggbb/0xrrggbb value 
	string.
*/
func ( dp *Qt_drawport ) Draw_text( x float64, y float64, value string, font string, cname string ) {
	var( 
		colour	*Gen_colour
	)

	if dp == nil {
		return
	}

	if cname == "" {
		colour = dp.colour
	} else {
		colour = Mk_gen_colour( cname, 255 )
	}

    t := dp.scene.AddText( value, dp.fonts.Get( font ) )
	t.SetPos2(  x, y  )
	t.SetDefaultTextColor( colour.Get_gui_colour() )
}

/*
	Draw a line from x1,y1 to x2,y2 using the indicated colour..
*/
func ( dp *Qt_drawport ) Draw_line( x1 float64, y1 float64, x2 float64, y2 float64, colour *Gen_colour ) {
	if dp == nil {
		return
	}

	pen := dp.get_pen( colour )
    dp.scene.AddLine2( x1, y1, x2, y2, pen )
}

/*
	Draw a rectangle  from with a lower left corner of x1, y1 and a height and width.
	Future: allow fill parms to be referenced with a name as a final parm.
*/
func ( dp *Qt_drawport ) Draw_rect( x1 float64, y1 float64, h float64, w float64, colour *Gen_colour ) {
	if dp == nil {
		return
	}

	pen := dp.get_pen( colour )
    dp.scene.AddRect2(  x1, y1, w, h,  pen, dp.fill )
}


/*
	Actually cause it to paint.
*/
func ( dp *Qt_drawport ) Paint( ) {
	if dp == nil {
		return
	}

	dp.Render()
	//dp.canvas.Show()    // this creates a new window?
}

/*
	Transfer the scene to the canvas.
*/
func ( dp *Qt_drawport ) Render( ) {
	if dp == nil {
		return
	}

    layout := widgets.NewQVBoxLayout()
    layout.AddWidget( dp.view, 0, 0 )

    dp.canvas.SetLayout( layout )
}



/*
	Set the current drawport colour.  The colour value (cv) may be a name (e.g. bluegreen) that 
	is known to the colour package, or a hex rgb value in either of the forms #rrggbb or 0xrrggbb.
*/
func ( dp *Qt_drawport ) Set_colour( cname string ) {
	if dp == nil {
		return
	}

	dp.colour = Mk_gen_colour( cname, 255 )
	dp.stylus = dp.get_pen( dp.colour )
}

/*
	Set the fill/background colour
func ( dp *Qt_drawport ) Set_fcolour( cname string ) {
	if dp == nil {
		return
	}

	dp.colour = Mk_gen_colour( cname, 255 )
	dp.stylus = dp.get_pen( dp.colour )
}
*/

