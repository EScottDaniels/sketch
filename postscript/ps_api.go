/*
	Mnemonic:	ps_api.go
	Abstract:	This module is the postscript implementation of the graphics interface.

				Structs used here should have a leading Gapi_ps_ prefix.

	Date:		22 April 2018
	Author:		E. Scott Daniels
*/
 		
package postscript 		
 		
import (  		
	"fmt" 		
	"os"
	"io"
 		
	clu "bitbucket.org/EScottDaniels/colour" 		
	sktools "bitbucket.org/EScottDaniels/sketch/tools" 		
 ) 		
 		
const (                  		
	PS_FL_SETUP		int = 1 << iota			// flag constants
	PS_FL_CLOSED
	PS_FL_FONTSET  
	PS_FL_GSAVED							// graphics is saved
 )                       		

// CAUTION: there is no way to reference these from the 'parent' without managing this directory
//			at the same level as sketch. So, these are defined in all places, and user will reference
//			them as sketch.FILL_*. These must match the constants in sketch.
const ( //  fill modes
	FILL_NONE int = iota
	FILL_SOLID
	FILL_OP_0			//  for postscript these are shades of grey from dark to light
	FILL_OP_1
	FILL_OP_2
	FILL_OP_3
	FILL_OP_4
	FILL_OP_5
	FILL_OP_6
	FILL_HORIZ
	FILL_VERT
)

/*
	Represents a sub-page (clipped region)
*/
type sub_page struct {
	xo	float64
	yo	float64
	height	float64
	width	float64
}
 		
/*
	Represents a page of output.
*/
type Ps_gc struct { 		
	colour string 			       		// last colour set (name or #rrggbb)
	fcolour string						// fill colour
	pcolour	string						// page colour
	fstyle	int							// fill style (set with fill_attr)
	font   string 			       		// last font set
	fsize  int    			       		// last font size that was set
	flags  int              		

	colours map[string]*clu.Colour		// colour map
	sub_pages map[string]*sub_page

	active_sp	*sub_page				// currently slected sub page

	states []state
	state_idx int
 		
	xscale	float64
	yscale  float64
	width  float64 		
	height float64 		
	output *os.File 		
} 		

/*
	Things saved when push state called.
*/
type state struct {
	colour string 			       		// last colour set (name or #rrggbb)
	fcolour string						// fill colour
	pcolour	string						// page colour
	fstyle	int							// fill style (set with fill_attr)
	font   string 			       		// last font set
	fsize  int    			       		// last font size that was set
	flags  int              		
}
 		
// ---------------- internal tools for ps --------------------------------------------------- 		

/*
	Save the things we must track when saving/restoring state.
	Returns true if state was successfully saved.
*/
func ( ps *Ps_gc ) save_state() ( bool ){
	if ps == nil {
		return false
	}

	if ps.states == nil {
		ps.states = make( []state, 25 )
		ps.state_idx = 0
	}

	if ps.state_idx >= 25 {
		return false
	}

	i := ps.state_idx
	ps.states[i].colour = ps.colour
	ps.states[i].fcolour = ps.fcolour
	ps.states[i].pcolour = ps.pcolour
	ps.states[i].fstyle = ps.fstyle
	ps.states[i].font = ps.font
	ps.states[i].fsize = ps.fsize
	ps.states[i].flags = ps.flags
	
	ps.state_idx++
	return true
}

func ( ps *Ps_gc ) restore_state() {
	if ps == nil || ps.states == nil {
		return
	}

	if ps.state_idx <= 0 {
		return
	}

	i := ps.state_idx - 1
	ps.colour = ps.states[i].colour 
	ps.fcolour = ps.states[i].fcolour 
	ps.pcolour = ps.states[i].pcolour 
	ps.fstyle = ps.states[i].fstyle 
	ps.font = ps.states[i].font 
	ps.fsize = ps.states[i].fsize 
	ps.flags = ps.states[i].flags 
	
	ps.state_idx--
}


/*
	See if we have a matching colour object and if not make one ans save it,
	Once we have one, return it's r, g, b value.
*/
func (ps *Ps_gc )  get_rgb( cname string ) ( r float64, g float64, b float64 )   {
	c := ps.colours[cname]
	if c == nil {
		c = clu.Mk_colour( cname )
		ps.colours[cname] = c
	}

	return c.Get_rgb_pct()
}

/*
	Set up the colour/grey value based on fill style. Returns true if fill was 
	set and a grestore must be added. If the style is not recognised, or is
	FILL_NONE, then no fill is set and false is returned.
*/
func ( ps *Ps_gc ) set_fill( style int ) ( bool ) {

	need_fill := false
	switch style {					// none, or unrecognised value is no fill
		case FILL_SOLID:
			need_fill = true
			r, g, b := ps.get_rgb( ps.fcolour )
			ps.printf( "gsave\n%.2f %.2f %.2f setrgbcolor\n", r, g, b )

		case FILL_OP_0:			// for postscript these are shades of grey 0 being dakest
			need_fill = true
			ps.printf( "gsave\n .125 setgray\n" )		// darkest

		case FILL_OP_1:
			need_fill = true
			ps.printf( "gsave\n .25 setgray\n" )

		case FILL_OP_2:
			need_fill = true
			ps.printf( "gsave\n .375 setgray\n" )

		case FILL_OP_3:
			need_fill = true
			ps.printf( "gsave\n .5 setgray\n" )

		case FILL_OP_4:
			need_fill = true
			ps.printf( "gsave\n .625 setgray\n" )

		case FILL_OP_5:
			need_fill = true
			ps.printf( "gsave\n .75 setgray\n" )

		case FILL_OP_6:
			need_fill = true
			ps.printf( "gsave\n .87.5 setgray\n" )		// lightest

		case FILL_HORIZ:

		case FILL_VERT:
	}

	return need_fill
}

/*
	Printf style write to the open output file.
*/
func( ps *Ps_gc ) printf( format string, va ...interface{} ) {
	io.WriteString( ps.output, fmt.Sprintf( format, va... ) )
}

/*
	Generate the initial setup if it hasn't already been written.
*/
func ( ps *Ps_gc ) write_setup(  ) { 		
	ps.printf( "%%%%BeginSetup\n" ) 		
	ps.printf( "mark {\n" ) 		
	ps.printf( "%%BeginFeature: *PageRegion C\n" ) 		
 		
	ps.printf( "<</PageSize [%.2f %.2f]>> setpagedevice\n", ps.width, ps.height ) 		
 		
	ps.printf( "%%EndFeature\n" ) 		
	ps.printf( "} stopped cleartomark\n" ) 		
	ps.printf( "%%%%EndSetup\n" ) 		
 		
	// needed if translating so that origin is at top left rather than bottom left 		
	ps.printf( "/xlate {0 %.2f translate} def\n/newp {showpage xlate} def\n", ps.height ) 		
 		
	ps.flags |= PS_FL_SETUP 		
	
} 		
 		
// ---------------- public --------------------------------------------------------------------
/*
	Allocate a postscript graphics construct.
	Output is the name of the output file that we'll write to.
*/
func Mk_ps_gc( output string ) (  gc *Ps_gc, err error ) { 		
	gc = &Ps_gc{} 		
 		
	gc.output, err = os.OpenFile( output, os.O_WRONLY|os.O_CREATE, 0644 ) 		
	if err != nil { 		
		return nil, fmt.Errorf( "mk_ps_gc: cannot oppen ooutput %s: %s", output, err ) 		
	} 		
	gc.output.Truncate( 0 )
 		
	gc.colour = "black"				// default colours
	gc.pcolour = "white"
	gc.width = 8.5 * 72.0				// standard letter size in points
	gc.height = 11.0 * 72
	gc.sub_pages = make( map[string]*sub_page, 17 )
	gc.colours = make( map[string]*clu.Colour, 17 )

	gc.printf( "%% ps_api.go generated this file\n" ) 		

	return gc, nil
} 		
 		
// ---- functions required to support the graphics_api interface ---------------------------- 		
 		
/*
	Clear the page by filling in the pcolour.
*/
func ( ps *Ps_gc ) Clear(  ) { 		
	if ps == nil {
		return
	}

	if ( ps.flags&PS_FL_SETUP ) == 0 { 			// not initialised yet
		ps.write_setup()
	} 		
 		
	r, g, b := ps.get_rgb( ps.pcolour )
	ps.printf( "gsave %.2f %.2f %.2f setrgbcolor\n", r, g, b )
	ps.printf( "0 0 moveto %.2f 0 rlineto 0 %2f rlineto -%2f 0 rlineto closepath fill\ngrestore\n", ps.width, ps.height, ps.width )
} 		
 		
/*
	Fill the subpage area with the indicated colour
*/
func ( ps *Ps_gc ) Clear_subpage( colour string ) { 		
	if ps == nil { 		
		return 		
	} 		

	subp := ps.active_sp
	if subp == nil {
		return
	}

	r, g, b := ps.get_rgb( colour )
	ps.printf( "gsave %.2f %.2f %.2f setrgbcolor\n", r, g, b )
	ps.printf( "0 0 moveto %.2f 0 rlineto 0 %2f rlineto -%2f 0 rlineto closepath fill\ngrestore\n", subp.width, subp.height, subp.width )
} 		

/*
	Close the output file and do any clean up needed. 
*/
func ( ps *Ps_gc ) Close( ) {
	if ps == nil {
		return
	}

	if ps.flags & PS_FL_CLOSED == PS_FL_CLOSED {
		return
	}

	ps.output.Close( )
	ps.flags |= PS_FL_CLOSED
}
 		
func ( ps *Ps_gc ) Delete_subpage( pname string ) { 		
	if ps == nil { 		
		return 		
	} 		

	if pname == "" {
		return
	}

	delete( ps.sub_pages, pname ) 
} 		

/*
	Draw an arc with center at xo, yo and a given radius.  alpha1 and alpha2 respectively are the starting
	and ending angles of the arc.  The angles are measured in degrees from "3 o'clock".
*/
func ( ps *Ps_gc ) Draw_arc( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64 ) {
	if ps == nil { 		
		return 		
	} 		

	ps.printf( "%.2f %.2f %.2f %.2f %.2f arc stroke\n", xo, yo, radius, alpha1, alpha2 )
}

/*
	Draw a circle with center at xo,yo and the given radius. The circle is filled depending on the 
	current value of fstyle. 
*/
func ( ps *Ps_gc ) Draw_circle( xo float64, yo float64, radius float64, outline bool ) {
	if ps == nil { 		
		return 		
	} 		

	if ps.set_fill( ps.fstyle ) {
		ps.printf( "%.2f %.2f %.2f 0 360 arc fill\ngrestore\n", xo, yo, radius )
	}

	if outline {
		ps.printf( "%.2f %.2f %.2f 0 360 arc stroke\n", xo, yo, radius )
	}
}


func ( ps *Ps_gc ) Draw_line( x1 float64, y1 float64, x2 float64, y2 float64 ) { 		
	if ps == nil { 		
		return 		
	} 		
 		
	ps.printf( "%.2f %.2f moveto %.2f %.2f lineto stroke\n", x1, y1, x2, y2 )
} 		

/*
	Draw image is a dummy function at the moment.
*/
func ( ps *Ps_gc ) Draw_image( xo float64, yo float64, iimage interface{} ) {
}

/*
	Draw a pie 'slice' with center at xo, yo and a given radius.  alpha1 and alpha2 respectively are the starting
	and ending angles of the arc.  The angles are measured in degrees from "3 o'clock". The outline and fill type
	parameters define whether the pie slice is outlined in the current colour and/or filled with the fill colour or
	grey scale using the FILL_* constants.
*/
func ( ps *Ps_gc ) Draw_pie( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64, outline bool ) {
	if ps == nil { 		
		return 		
	} 		

	if ps.set_fill( ps.fstyle ) {
		ps.printf( "%.2f %.2f moveto %.2f %.2f %.2f %.2f %.2f arc closepath fill\ngrestore\n", xo, yo, xo, yo, radius, alpha1, alpha2 )
	}

	if outline {
		ps.printf( "%.2f %.2f moveto %.2f %.2f %.2f %.2f %.2f arc closepath stroke\n", xo, yo, xo, yo, radius, alpha1, alpha2 )
	}
}
 		
/*
	Draw a rectangle, x,y is the lower left cornoer.
*/
func ( ps *Ps_gc ) Draw_rect( xo float64, yo float64, height float64, width float64, outline bool ) { 		
	if ps == nil { 		
		return 		
	} 		

	if ps.set_fill( ps.fstyle ) {
		ps.printf( "%.2f %.2f moveto %.2f 0 rlineto 0 %.2f rlineto %.2f 0 rlineto closepath fill\ngrestore\n", xo, yo, width, height, -width )
	}

	if outline {
		ps.printf( "%.2f %.2f moveto %.2f 0 rlineto 0 %.2f rlineto %.2f 0 rlineto closepath stroke\n", xo, yo, width, height, -width )
	}
} 		

/*
	Draw_poly accepts a slice of sktools:points and draws the polygon formed by the points. We
	assume that the first point and the last point are to be 'closed'. The poly is filled based
	on the current fill style (not if none is set) and outlined if outline is true.
*/
func ( ps *Ps_gc ) Draw_poly( points []*sktools.Point, outline bool ) { 		
	if ps == nil { 		
		return 		
	} 		

	if len( points ) < 2 {
		return
	}

	if ps.set_fill( ps.fstyle ) {			
		for i, point := range points {
			x, y := point.Get_xy( )

			if i == 0 {
				ps.printf( "%.2f %.2f moveto ", x, y )
			} else {
				ps.printf( "%.2f %.2f lineto ", x, y )
			}	
		}

		ps.printf( "closepath fill\ngrestore\n" ) 		// set_fill does a gsave if the fill is set, so we must to a retore after filling
	}

	if outline {
		for i, point := range points {
			x, y := point.Get_xy( )

			if i == 0 {
				ps.printf( "%.2f %.2f moveto ", x, y )
			} else {
				ps.printf( "%.2f %.2f lineto ", x, y )
			}	
		}

		ps.printf( "closepath stroke\n" )
	}
} 		

/*
	Draw text at xo,yo with the current font, size and colour settings.
*/
func ( ps *Ps_gc ) Draw_text( xo float64, yo float64, text string ) { 		
	if ps == nil { 		
		return 		
	} 		

	if ps.flags & PS_FL_FONTSET != PS_FL_FONTSET {
		ps.printf( "(%s) findfont %d scalefont setfont\n", ps.font, ps.fsize )
		ps.flags |= PS_FL_FONTSET
	}

	ps.printf( "%.2f %.2f moveto (%s) show\n", xo, yo, text )
} 		

func ( ps *Ps_gc ) Engague(  ) { 		
	if ps == nil { 		
		return 		
	} 		
} 		

func ( ps *Ps_gc ) Mk_page( pname string, h int, w int, title string ) ( error ) { 		
	if ps == nil {                                                		
		return fmt.Errorf( "ps.wk_page: nil pointer passed" )							 		//  comment here
	}                                                             		

	return nil
} 		

func ( ps *Ps_gc ) Mk_subpage( pname string, xo float64, yo float64, height float64, width float64 ) {
	if ps == nil { 		
		return 		
	} 		

	if pname == "" {
		return
	}

	sp := &sub_page{
		xo:	xo,
		yo:	yo,
		height: height,
		width: width,
	}

	ps.sub_pages[pname] = sp
} 		

/*
	Mk_locked_subpage creates a subpage that is locked in place which really only applies to the 
	sketch interfaces that are window/screen oriente. 
*/
func ( ps *Ps_gc ) Mk_locked_subpage( pname string, xo float64, yo float64, height float64, width float64 ) {
	ps.Mk_subpage( pname, xo, yo, height, width )
}

/*
	restore the previously saved graphics state.
*/
func ( ps *Ps_gc ) Pop_state ( ) {
	if ps == nil {
		return
	}

	ps.printf( "grestore\n" )
	ps.restore_state()			// restore internal state
}

/*
	Save the current graphics state.
*/
func ( ps *Ps_gc ) Push_state ( ) {
	if ps == nil {
		return
	}

	ps.printf( "gsave\n" )
	ps.save_state()				// save our state
}


/*
	Set the rotation n degrees from "3 o'clock".

	Returns true to indicate that it was successful.
*/
func ( ps *Ps_gc ) Rotate( degrees float64 )  ( bool ){ 		
	if ps == nil { 		
		return false
	} 		
	
	ps.printf( "%.2f rotate\n", degrees )
	return true
} 		

func ( ps *Ps_gc ) Select_page( pname string ) { 		
	if ps == nil { 		
		return 		
	} 		
} 		

/*
	Selects (activates) the 'sub page' (clip area) by name.  If the name "default" is
	given the main page is selected.  The origin of the sub-page is the lower left corner
	of the page. Returns true if the subpage was found.
*/
func ( ps *Ps_gc ) Select_subpage( pname string ) ( bool ) {
	if ps == nil { 		
		return false
	} 		

	if ps.flags & PS_FL_GSAVED == PS_FL_GSAVED {
		ps.printf( "grestore\n" )
		ps.flags &= ^PS_FL_GSAVED
	}

	ps.active_sp = nil
	if pname == "default" {			// main page selected 
		return true
	}

	sp := ps.sub_pages[pname]
	if sp == nil {
		return false
	}

	ps.active_sp = sp
	ps.flags |= PS_FL_GSAVED
	ps.printf( "gsave\n%.2f %.2f translate\n", sp.xo, sp.yo )
	ps.printf( "0 0  moveto %.2f 0 rlineto 0 %.2f rlineto %.2f 0 rlineto closepath clip newpath\n", sp.width, sp.height, -sp.width )

	if ps.fstyle != FILL_NONE {
		ps.Set_colour( ps.fcolour )
		ps.printf( "0 0  moveto %.2f 0 rlineto 0 %.2f rlineto %.2f 0 rlineto closepath fill\n", sp.width, sp.height, -sp.width )
	}

	return true
} 		


/*
	Postscript sets ital and 'weight' using font names, not values. This
	function assumes that the name has been converted to the proper name
	(e.g. Helvetica-bold) before calling.
*/
func ( ps *Ps_gc ) set_ps_font( fname string, points int ) {
	if fname == ps.font && ps.fsize == points {					// already set, nothing to do
		return
	}

	if ps.font != fname || ps.fsize != points {
		ps.font = fname
		ps.fsize = points
		ps.flags &= ^PS_FL_FONTSET			// turn off the font already set flag; next draw text will set
	}
}

/*
	Set the fill attributes: colour and style.
*/
func ( ps *Ps_gc ) Set_fill_attrs( cname string, fill_style int ) {
	if ps == nil { 		
		return 		
	} 		

	ps.fcolour = cname
	ps.fstyle = fill_style
}

/*
	Set the desired font.  For postscript a weight of 0 or 1 is normal and 
	>1 is bold. If ital is true, then -oblique is added to the font name.
*/
func ( ps *Ps_gc ) Set_font( fname string, points int, weight int, ital bool ) { 		
	if ps == nil { 		
		return 		
	} 		

	switch( fname ) {				// allow for generic names
		case "serif", "Serif":	
			if weight > 1 || ital {
				fname = "Times"
			} else {
				fname = "Times-Roman"
			}

		case "sans", "sans-serif", "Sans", "Sans-serif":
			fname = "Helvetica"

		case "fixed", "Fixed", "cw":
			fname = "Courier"
	}

	if ital {
		if weight > 1 {
			fname += "-BoldOblique"
		} else {
			fname += "-Oblique"
		}
	} else {
		if weight > 1 {
			fname += "-Bold"
		}
	}
	
	ps.set_ps_font( fname, points )
} 		

/*
	Set the named colour as the current colour.
*/
func ( ps *Ps_gc ) Set_colour( cname string ) { 		
	if ps == nil { 		
		return 		
	} 		

	if ps.colour == cname {		// still set from their last call, no need for action
		return
	}

	ps.colour = cname
	r, g, b := ps.get_rgb( cname )

	ps.printf( "%.3f %.3f %.3f setrgbcolor\n", r, g, b )
} 		

/*
	Set the style that lines will be drawn in.
	Pattern is an array of integers which are used asn an on/off
	pattern for setting a dashed line. [3] sets 3 on, 3 off while
	[2, 3] sets two on, three off.

	Passing a nil array sets a solid line style.
*/
func ( ps *Ps_gc ) Set_line_style( pattern []int ) {
	if ps == nil { 		
		return 		
	} 		

	if pattern == nil {
		ps.printf( "[] 0 setdash\n" )		// solid line
		return
	}

	ps.printf( "[ " )
	for i := 0; i < len( pattern ); i++ {
		ps.printf( "%d ", pattern[i] )
	}

	ps.printf( "] 0 setdash\n" )

}

/*
	Set the line width.  A value of 0 is the smalles possible width supported by the device.
*/
func ( ps *Ps_gc ) Set_line_width( width int ) {
	if ps == nil { 		
		return 		
	} 		

	ps.printf( "%d setlinewidth\n", width )
}

/*
	Set the named colour as the page colour.
*/
func ( ps *Ps_gc ) Set_page_colour( cname string ) { 		
	if ps == nil || cname == "" { 		
		return 		
	} 		

	ps.pcolour = cname
} 		

/*
	Set the scale factor.
*/
func ( ps *Ps_gc ) Set_scale( xscale float64, yscale float64 ) {
	if ps == nil {
		return 		
	} 		

	ps.xscale = xscale
	ps.yscale = yscale
	ps.printf( "%.2f %.2f scale\n", xscale, yscale )
}

/*
	Adjust the scale by the increment value.
*/
func ( ps *Ps_gc ) Set_iscale( increment float64 ) {
	if ps == nil {
		return 		
	} 		

	ps.xscale += increment
	ps.yscale += increment
	ps.printf( "%.2f %.2f scale\n", ps.xscale, ps.yscale )
}

/*
	Set the dimensions of the page (width x height).
*/
func ( ps *Ps_gc ) Set_dimensions( width float64, height float64 ) {
	if ps == nil {
		return 		
	} 		

	ps.width = width
	ps.height = height
} 		

/*
	Cause what ever has been drawn onto the page to be rendered.
*/
func ( ps *Ps_gc ) Show( ) {
	if ps == nil { 		
		return 		
	} 		

	ps.printf( "showpage\n" )
	ps.Set_colour( ps.colour )		// reset colour
	ps.fsize = 0					// force font reset
	ps.flags &= ^PS_FL_GSAVED
}

/*
	Translate adjusts the orign to x,y.
*/
func ( ps *Ps_gc ) Translate( x float64, y float64 ) {
	if ps == nil { 		
		return 		
	} 		
	
	ps.printf( "%.2f %.2f translate\n", x, y )
} 		

/*
	Translate_delta adjusts the orign by delta_x, delta_y.
	??? is this accurate?
*/
func ( ps *Ps_gc ) Translate_delta( delta_x float64, delta_y float64 ) {
	if ps == nil { 		
		return 		
	} 		
	
	ps.printf( "%.2f %.2f translate\n", delta_x, delta_y )
} 		


// not implemented just yet
func ( ps *Ps_gc ) Mk_scaled_image( fname string, scale float64 ) ( interface{}, error ) {
	return "[image not implemented by postscript]", nil
}

func ( ps *Ps_gc ) Mk_bounded_image( fname string, max_height int, max_width int ) ( interface{}, error ) {
	return "[image not implemented by postscript]", nil
}

func ( ps *Ps_gc ) Mk_stacked_image( fnames []string, max_height int, max_width int ) ( interface{}, error ) {
	return "[stacked image not implemented by postscript]", nil
}

/*
	Some environments must release resources associated with images. For PS this is 
	probably not needed, but we must maintain the hook for the interface
*/
func ( ps *Ps_gc ) Close_image( iimage interface{} ) {
}


// --- interactive things just don't have a place, but must be supported because of GO's interface rules ---


func (ps *Ps_gc ) Add_button( bid int, x int, y int, height int, width int, label string, colour string, txt_colour string, kind int, pushed bool ) {
}

/*
	Sets the interactive event listener for this gc. This function also 
	"triggers" the callback registration.
*/
func ( ps *Ps_gc ) Add_listener(  ch chan *sktools.Interaction ) {
}

func ( ps *Ps_gc ) Drive() {
}

/*
*/
func ( ps *Ps_gc ) Release_button( subpage_name string, button_id int ) {
}

/*
*/
func ( ps *Ps_gc ) Press_button( subpage_name string, button_id int ) {
}
