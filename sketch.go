/*
	Abstract:	A generic graphics api (interface) which provides a layer betwwen the overly complicated (e.g. QT) or
				relatively misunderstood (e.g. Postscript) rengering agents and higher level things like views and
				drawing things.

				Using the Mk_graphics_api creates the underlying struct and makes the interface functions
				available to the creating process.

	Date:		21 April 2018
	Author:		E. Scott Daniels
*/
 		
package sketch 		
 		
import (  		
	"fmt" 		
 		
	ps "bitbucket.org/EScottDaniels/sketch/postscript" 		
	"bitbucket.org/EScottDaniels/sketch/xigo" 		
	sktools "bitbucket.org/EScottDaniels/sketch/tools" 		

	//"github.com/ScottDaniels/sketch/qt" 		
 ) 		
                      		
const (               		//        fill styles
	FILL_NONE int = iota 		
	FILL_SOLID 		
	FILL_OP_0  		//        opaque level 0
	FILL_OP_1  		
	FILL_OP_2 		
	FILL_OP_3 		
	FILL_OP_4 		
	FILL_OP_5 		
	FILL_OP_6 		
	FILL_HORIZ 		
	FILL_VERT 		
 ) 		

/*
	CAUTION: to allow for xigo to be a separate package under this directory these
	must be duplicated here; match the order with xigo filesor odd things will happen.
*/
const (
	_				int = iota		// XI doesn't assign button type 0
	BUTTON_RADIO	
	BUTTON_STICKY	
	BUTTON_SPRING
	BUTTON_ENTRY
)
 		
/*
	This defines the interface that a graphics api class must provide.
*/
type Graphics_api interface { 		
	Clear(  ) 		
	Clear_subpage( colour string ) 		
	Close(  ) 		
 		
	Delete_subpage( pname string ) 		
	Draw_arc( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64 ) 		
	Draw_circle( xo float64, yo float64, radius float64, outline bool ) 		
	Draw_image( xo float64, yo float64, iimage interface{} )
	Draw_line( x1 float64, y1 float64, x2 float64, y2 float64 ) 		
	Draw_pie( xo float64, yo float64, radius float64, alpha1 float64, alpha2 float64, outline bool ) 		
	Draw_poly( points []*sktools.Point, outline bool ) 		
	Draw_rect( xo float64, yo float64, height float64, width float64, outline bool ) 		
	Draw_text( xo float64, yo float64, text string ) 		
 		
	Engague(  ) 		
	Mk_page( pname string, h int, w int, title string ) error 		
	Mk_subpage( pname string, xo float64, yo float64, height float64, width float64 ) 		
	Mk_locked_subpage( pname string, xo float64, yo float64, height float64, width float64 ) 		
	Pop_state(  ) 		
	Push_state(  ) 		
	Rotate( degrees float64 ) ( bool )
	Select_page( pname string ) 		
	Select_subpage( pname string ) ( bool )
 		
	Set_fill_attrs( cname string, fill_style int ) 		
	Set_font( fname string, points int, weight int, ital bool ) 		
	Set_colour( cname string ) 		
	Set_dimensions( height float64, width float64 ) 		
	Set_iscale( increment float64 )
	Set_line_style( pattern []int ) 		
	Set_line_width( width int ) 		
	Set_page_colour( cname string ) 		
	Set_scale( xscale float64, yscale float64 ) 		

	// --- these might not be implemented by all underlying gc providers ---
	Mk_scaled_image( fname string, scale float64 ) ( iimage  interface{}, e error )
	Mk_bounded_image( fname string, max_height int, max_width int ) ( iimage interface{}, e error )
	Mk_stacked_image( fnames []string, max_height int, max_width int ) ( iimage interface{}, e error )
	Close_image( iimage interface{} )

	// ---- interactive elements; not implemented by all underlying gc providers --
	Add_button( bid int, x int, y int, height int, width int, label string, colour string, txt_colour string, kind int, pushed bool )
	Add_listener( lchan chan *sktools.Interaction )
	Press_button( subpage_name string, button_id int )
	Release_button( subpage_name string, button_id int )
	Drive()
 		
	Show(  ) 		
	Translate( x float64, y float64 ) 		
	Translate_delta( delta_x float64, delta_y float64 ) 		
} 		
 		
/*
	Initialise a graphics interface based on the kind string. Currently supported
	interfaces are "qt" and "ps" or "postscript".
*/
func Mk_graphics_api( kind string, output string ) ( Graphics_api, error ) { 		
 		
	switch kind { 		
/*
	case "qt", "QT": 		
		gapi := qt.Mk_qt_gc(  ) 		
		return gapi, nil 		
*/
 		
	case "xi", "xwin", "XI":
		capi, err := xigo.Mk_xigo_gc( output );	// output for xi is of the form: "display.screen,height,width,title"
		return capi, err

	case "ps", "postscript", "Postscript": 		
		gapi, err := ps.Mk_ps_gc( output ) 		
		return gapi, err 		
 		
	default: 		
		return nil, fmt.Errorf( "unsupported graphics api kind: %s", kind ) 		
	} 		
} 		
