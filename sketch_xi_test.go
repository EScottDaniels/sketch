/*
	Mnemonic:	sketch_xi_test.go
	Abstract:	Basic unit test for XI sketch interface.
	Author:		E. Scott Daniels
	Date:		24 July 2018
*/

package sketch

import (
	"fmt"
	"os"
	"time"
	"testing"
)

/*
	This is a basic test of the xi API. 
*/
func Test_xi_api(t *testing.T) {
	gc, err := Mk_graphics_api( "xi", ",900,1200,XI Sketch Test")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[FAIL] cannot create xi construct: %s\n", err)
		t.Fail()
		return
	}
	fmt.Fprintf(os.Stderr, "[OK]  created xi construct\n" );

	//italic_style := true
	upright_style := false
	//bold_weight := 3
	//normal_weight := 0


	//gc.Set_scale( 2, 2 )
	gc.Set_dimensions( 8.5 * 72.0, 110 * 72 )		// should have no effect in xi.
	gc.Select_subpage( "root" )
	gc.Set_page_colour( "black" )
	gc.Set_colour( "orange" )
	gc.Set_font("Serif", 14, 1, upright_style )

	gc.Mk_subpage( "c1", 100, 10, 500, 800 )
	gc.Mk_subpage( "c2", 400, 400, 500, 800 )

	gc.Select_subpage( "c1" )
	gc.Set_page_colour( "#000030" )

	gc.Select_subpage( "c2" )
	gc.Set_page_colour( "#303000" )

	gc.Clear( )

	// this should be partially covered by the subpage
	gc.Select_page( "root" )
	gc.Set_colour( "#a09640" )
	gc.Draw_text( 10, 800, "Page colour was set to 0x003030, about to draw a diagional line from 0,0 to 400,400" )
	gc.Draw_line(0, 0, 400, 400)

	// ----- font test -----------------------------------
	sleep_period := time.Duration( 5 * time.Second )

	for i:= 0; i < 2; i++ {

		gc.Select_subpage( "c1" )
		gc.Set_colour( "green" )
		ly := run_font( gc, 200, 20, "Sans", false, false )
		ly = run_font( gc, 200, ly, "Serif", false, false )
		run_font( gc, 200, ly, "Fixed", false, false )

		gc.Select_subpage( "c2" )
		gc.Set_colour( "blue" )
		ly = run_font( gc, 200, 20, "Sans", false, false )
		ly = run_font( gc, 200, ly, "Serif", false, false )
		run_font( gc, 200, ly, "Fixed", false, false )

		gc.Show()
		gc.Clear()
		time.Sleep( sleep_period  )
		sleep_period = 1 * time.Second
	
		gc.Select_subpage( "c1" )
		gc.Set_colour( "#f05020" )
		ly = run_font( gc, 200, 20, "Sans", true, false )
		ly = run_font( gc, 200, ly, "Serif", true, false )
		run_font( gc, 200, ly, "Fixed", true, false )

		gc.Select_subpage( "c2" )
		gc.Set_colour( "pink" )
		ly = run_font( gc, 200, 20, "Sans", true, false )
		ly = run_font( gc, 200, ly, "Serif", true, false )
		run_font( gc, 200, ly, "Fixed", true, false )

		gc.Show()
		gc.Clear()
		time.Sleep( sleep_period  )
/*
		gc.Clear_subpage( "black" )
		ly = run_font( gc, 200, 20, "Sans", false, true )
		ly = run_font( gc, 200, ly, "Serif", false, true )
		run_font( gc, 200, ly, "Fixed", false, true )
		gc.Show()
		time.Sleep( sleep_period  )
		gc.Clear( )

		//gc.Clear_subpage( "black" )
*/
	}

	gc.Select_page( "default" )
	gc.Set_colour( "white" )
	run_font( gc, 600, 500, "Sans", false, false )
	gc.Clear()

	gc.Set_colour( "0x9000b0" )
	gc.Push_state()
	gc.Rotate(45.0)
	gc.Draw_text(20, 20, "Sub-page text: Now is the time for all good programmers to buck it up and learn Postscript!")
	gc.Pop_state()
	gc.Set_fill_attrs("white", FILL_NONE)
	gc.Draw_rect(10, 10, 300, 300, true)

	gc.Select_subpage("default")

	gc.Set_colour( "0x000090" )
	gc.Draw_rect(25, 300, 100, 300, true)
	gc.Set_fill_attrs("#000040", FILL_OP_5)
	gc.Draw_rect(25, 410, 100, 300, false)

	gc.Set_fill_attrs("#f00040", FILL_SOLID)	// causes the NEXT page to be coloured with this colour
	gc.Show()
	time.Sleep( 5 * time.Second )


	gc.Set_page_colour("#000000")
	gc.Clear()
	gc.Set_colour("orange")
	gc.Push_state()
	gc.Set_colour("white")

	gc.Set_fill_attrs("#f000f0", FILL_SOLID)
	gc.Draw_arc(100, 100, 50, 0, 45)
	gc.Draw_pie(100, 300, 50, 25, 45, true)
	gc.Set_fill_attrs("#00ff00", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 45, 140, true)
	gc.Set_fill_attrs("#f00000", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 140, 270, true)
	gc.Set_fill_attrs("#f090f0", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 270, 360, true)
	gc.Set_fill_attrs("maroon", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 0, 25, true)
	gc.Pop_state()

	gc.Set_colour("white")
	gc.Set_fill_attrs("white", FILL_NONE)
	gc.Draw_circle(100, 300, 50, true)

	gc.Set_colour("white")
	gc.Set_line_width(3)

	gc.Select_subpage( "c2" )
	style := make([]int, 2)
	style[0] = 6
	style[1] = 2
	gc.Set_line_style(style) 					// 4 on, 2 off
	gc.Draw_line(175, 300, 300, 300)

	gc.Set_line_width(0)
	style = make([]int, 4)
	style[0] = 6             // dash			// for xi the pattern is meaningless, but indicates a dashed line
	style[1] = 3             // space
	style[2] = 2             // dot
	style[3] = 3             // space
	gc.Set_line_style( style ) // dash dot dot dash
	gc.Draw_line(175, 310, 300, 310)


	gc.Set_line_style(nil) // reset to solid line
	gc.Draw_line(175, 320, 300, 320)


	// -------------------------------------------------------------------

	gc.Set_fill_attrs( "green", FILL_SOLID )
	gc.Show()
	time.Sleep( 15 * time.Second )
	gc.Close()
}

func run_font( gc Graphics_api, x float64, y float64, style string, bold bool, ital bool ) (float64) {
	fsize := 8
	bold_flag := 0
	bold_str := "normal"
	if bold {
		bold_flag = 3
		bold_str = "bold"
	}
	ital_str := ""
	if ital {
		ital_str = "italic"
	}

	for i := 0; i < 10; i++ {
		text := fmt.Sprintf( "Text style %s %d points %s %s", style, fsize, bold_str, ital_str )
		gc.Set_font( style, fsize, bold_flag, ital )
		gc.Draw_text( x, y, text )

		y += float64( fsize ) + 4
		fsize += 2
	}
	
	return y
}
