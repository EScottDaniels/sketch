/*
	Mnemonic:	sketch_test.go
	Abstract:	Basic unit test for sketc interface.
	Author:		E. Scott Daniels
	Date:		3 May 2018
*/

package sketch

import (
	"fmt"
	"os"
	"testing"
)

/*
	This is a basic test of the postscript API. It generates test1.ps. These are
	pretty much the same tests in the postscirpt directory, but prove that it can
	be done via the generic api.
*/
func TestPostscript_api(t *testing.T) {
	gc, err := Mk_graphics_api("postscript", "test1.ps")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[FAIL] cannot create postscript construct with output for test1.ps: %s\n", err)
		t.Fail()
		return
	}

	fmt.Fprintf(os.Stderr, "[OK]  created postscript construct with output for test1.ps\n")

	//gc.Set_dimensions( 8.5 * 72.0, 110 * 72 )
	gc.Set_dimensions(11.0*72.0, 8.5*72)
	gc.Set_colour("#000000")
	gc.Set_page_colour("#fefe00")
	gc.Clear() // setup and render page bg colour
	gc.Set_font("Helvetica", 12, 1, false)
	gc.Draw_text(100, 100, "Now is the time for all good programmers to buck it up and learn Postscript!")

	gc.Draw_line(0, 0, 400, 400)

	gc.Mk_subpage("c1", 125, 125, 300, 300)
	gc.Select_subpage("c1")
	gc.Clear_subpage("#e000e0")

	gc.Push_state()
	gc.Rotate(45.0)
	gc.Draw_text(0, 0, "Sub-page text: Now is the time for all good programmers to buck it up and learn Postscript!")
	gc.Pop_state()
	gc.Set_fill_attrs("white", FILL_NONE)
	gc.Draw_rect(10, 10, 300, 300, true)

	gc.Select_subpage("default")

	gc.Draw_rect(25, 300, 100, 300, true)
	gc.Set_fill_attrs("grey", FILL_OP_5)
	gc.Draw_rect(25, 410, 100, 300, false)

	gc.Set_page_colour("#000000")
	gc.Show()
	gc.Clear() // render the page colour

	gc.Set_colour("orange")
	gc.Push_state()
	gc.Set_colour("white")

	gc.Set_fill_attrs("#f000f0", FILL_SOLID)
	gc.Draw_arc(100, 100, 50, 0, 45)
	gc.Draw_pie(100, 300, 50, 25, 45, true)
	gc.Set_fill_attrs("#00ff00", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 45, 140, true)
	gc.Set_fill_attrs("#f00000", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 140, 270, true)
	gc.Set_fill_attrs("#f090f0", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 270, 360, true)
	gc.Set_fill_attrs("maroon", FILL_SOLID)
	gc.Draw_pie(100, 300, 50, 0, 25, true)
	gc.Pop_state()

	gc.Set_colour("white")
	gc.Set_fill_attrs("white", FILL_NONE)
	gc.Draw_circle(100, 300, 50, true)

	gc.Set_colour("yellow")
	gc.Set_line_width(3)

	style := make([]int, 2)
	style[0] = 6
	style[1] = 2
	gc.Set_line_style(style) // 4 on, 2 off
	gc.Draw_line(175, 300, 300, 300)

	gc.Set_line_width(0)
	style = make([]int, 4)
	style[0] = 6             // dash
	style[1] = 3             // space
	style[2] = 2             // dot
	style[3] = 3             // space
	gc.Set_line_style(style) // dash dot dot dash
	gc.Draw_line(175, 310, 300, 310)

	gc.Set_line_style(nil) // reset to solid line
	gc.Draw_line(175, 320, 300, 320)

	gc.Set_font("Sans", 12, 0, false)
	gc.Draw_text(200, 10, "this is text in Sans font")
	gc.Set_font("Serif", 12, 0, false)
	gc.Draw_text(200, 20, "this is text in Serif font")
	gc.Set_font("Fixed", 12, 0, false)
	gc.Draw_text(200, 30, "this is text in Serif font")

	gc.Set_font("Sans", 12, 2, false)
	gc.Draw_text(400, 10, "this is text in Sans bold font")
	gc.Set_font("Serif", 12, 2, false)
	gc.Draw_text(400, 20, "this is text in Serif bold font")
	gc.Set_font("Fixed", 12, 2, false)
	gc.Draw_text(400, 30, "this is text in fixed bold font")

	gc.Set_font("Sans", 12, 0, true)
	gc.Draw_text(400, 40, "this is text in Sans ital font")
	gc.Set_font("Serif", 12, 0, true)
	gc.Draw_text(400, 50, "this is text in Serif ital font")
	gc.Set_font("Fixed", 12, 0, true)
	gc.Draw_text(400, 60, "this is text in fixed ital font")

	gc.Show()
	gc.Close()
}
