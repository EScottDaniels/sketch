
/*
	A generic interaction -- something that happened at a given x,y coordinate
	in a given drawing space. 
*/

package tools

import (
	"fmt"
)

/*
	Event type constants.
*/
const (
	IK_SB_PRESS		int = iota		// softbutton event
	IK_SB_RELEASE	
	IK_M_PRESS						// mouse hard button press/release
	IK_M_RELEASE	
	IK_M_MOVED						// mouse movement
	
)

type Interaction struct {
	X float64
	Y float64
	Kind int					// one of the IK_* constants
	Data	int					// data specific to the event (soft button id, mouse button etc.)
}


/*
	To_string generates a simple string representation of the instance.
*/
func ( inst *Interaction ) To_string( ) ( string ) {
	if inst == nil {
		return "<nil>"
	}

	return fmt.Sprintf( "(%.2f,%.2f) kind=%d data=%d", inst.X, inst.Y, inst.Kind, inst.Data )	
}

