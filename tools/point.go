
package tools

/*
	An x,y point.
*/
type Point struct {
	x	float64
	y	float64
}

func Mk_point( x float64, y float64 ) ( *Point ) {
	pt := &Point {
		x:	x,
		y:	y,
	}

	return pt
}

func ( pt *Point ) Get_xy( ) ( x float64, y float64 ) {
	if pt == nil {
		return 0,0
	}

	return pt.x, pt.y
}
