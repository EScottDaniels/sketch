module bitbucket.org/EScottDaniels/sketch

go 1.18

require (
	bitbucket.org/EScottDaniels/colour v1.2.0
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d
	gitlab.com/rouxware/goutils/clike v1.0.0
)

require github.com/gopherjs/gopherjs v0.0.0-20190411002643-bd77b112433e // indirect
